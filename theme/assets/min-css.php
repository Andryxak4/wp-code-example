<?php

$files = glob(__DIR__.'/css/*.css');

$saved_total = 0;
$total_before = 0;
$all_css_filename = __DIR__ . '/css/all.css';
if (file_exists($all_css_filename)):
    $all_css_content = file_get_contents($all_css_filename);
else:
    $all_css_content = '';
endif;
$all_css_is_empty = empty($all_css_content);
foreach ($files as $filename):
    $basename = basename($filename);
    if (($basename=='all.css') || (!$all_css_is_empty && preg_match('/\.min\.css$/', $basename))):
        continue;
    endif;
    echo "  Processing file \e[96m$basename\e[39m...\n";
    $content = file_get_contents($filename);
    $minified_content = str_replace(["\r", "\n"], ['', ''], $content);              // remove new line characters
    $minified_content = preg_replace('/\s{2,}/', ' ', $minified_content);           // replace multiple spaces by single one
    $minified_content = preg_replace('/\s*([{}:;])\s*/', '$1', $minified_content);  // remove spaces around {}:; characters
    $minified_content = preg_replace('#\s*(/\*|\*/)\s*#', '$1', $minified_content); // remove spaces around comments
    
    $size_before = strlen($content);
    $size_after = strlen($minified_content);
    $total_before += $size_before;
    echo "    saved \e[32m".(100-round(100*$size_after/$size_before, 3))."%\e[39m ($size_before => $size_after bytes)\n";
    
    $filename_min = str_replace('.css', '.min.css', $filename);
    file_put_contents($filename_min, $minified_content);
    $saved_total += ($size_before - $size_after);

    $regexp = "#/\*$basename\*/\r?\n.*?\r?\n#";
    if ($all_css_is_empty || !preg_match($regexp, $all_css_content)):
        $all_css_content .= "/*$basename*/\n$minified_content\n";
    else:
        $all_css_content = preg_replace($regexp, "/*$basename*/\n$minified_content\n", $all_css_content);
    endif;
endforeach;
file_put_contents($all_css_filename, $all_css_content);
$saved_total_percent = $total_before ? round(100 * $saved_total / $total_before, 3) : 0;
echo "Total saved: \e[92m$saved_total_percent%\e[39m (\e[97m$saved_total\e[39m of \e[97m$total_before\e[39m bytes)\n";
