var head=document.getElementsByTagName('head')[0];
var calendar;
$(function() {
  var calendarEl = document.getElementById('calendar');

  var calendarSources = {
    alarms: {
      id: 'alarms',
      url: chamberonne.ajaxurl + '?action=getCalendarEventsAlarms',
      color: '#EB061E'
    },
    activities: {
      id: 'activities',
      url: chamberonne.ajaxurl + '?action=getCalendarEventsActivities'
    },
    miscellaneous: {
      id: 'miscellaneous',
      url: chamberonne.ajaxurl + '?action=getCalendarEventsMiscellaneous',
      color: 'gray'
    },
    officers: {
      id: 'officers',
      url: chamberonne.ajaxurl + '?action=getCalendarEventsOfficers'
    },
    vehicles: {
      id: 'vehicles',
      url: chamberonne.ajaxurl + '?action=getCalendarEventsVehicles'
    }
  };

  calendar = new FullCalendar.Calendar(calendarEl, {
    locale: 'fr',
    plugins: [ 'interaction', 'dayGrid' ],
    header: {
      left: 'prev,next',
      center: 'title',
      right: 'export'
    },
    fixedWeekCount: false,
//    showNonCurrentDates: false,
    eventLimit: true, // allow "more" link when too many events
    eventSources: [
      calendarSources.alarms,
      calendarSources.activities,
      calendarSources.miscellaneous,
      calendarSources.vehicles
    ],
    customButtons: {
      'export': { text: 'Exporter' }
    }
  });

  calendar.render();

  $('.calendar-filters .btn').click(function(e) {
    e.preventDefault();
    var $this = $(this);
    var filter = $this.data('filter');
    if ($(this).hasClass('active')) {
      var es = calendar.getEventSourceById(filter);
      es.remove();
    } else {
      calendar.addEventSource(calendarSources[filter]);
    }
    $this.toggleClass('active');
  });

  function showSuccessMessageAfterExport() {
    $('#calendar-export .success').addClass('active');
    setTimeout(function(){
      $.fancybox.close();
    }, 2000);
  }

  $(document).on('click', '#calendar .fc-right .fc-export-button', function() {

    $('#calendar-export .form-checkbox input').each(function(){
      $(this).prop('checked', true);
    });
    $('#calendar-export .success').removeClass('active');

    $.fancybox({
        type: 'inline',
        href: '#calendar-export'
    });
  });

  $(document).on('click','#calendar-export button[type=submit]', function(e){
    e.preventDefault();

    var checkboxes = [];
    $('#calendar-export .form-checkbox input:checked').each(function(){
      checkboxes.push(this.name);
    });
    window.open('/sdis-calendar-events?filter[]='+checkboxes.join('&filter[]='));
  });

});
