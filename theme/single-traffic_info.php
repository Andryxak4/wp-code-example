<?php
get_header();
$general_settings = Chamberonne::getGeneralSettings();
while (have_posts()):
  the_post();
  $fields = get_fields();
  $banner = $fields['banner']
                ?: $general_settings['single_traffic_info_banner']
                ?: $general_settings['traffic_info_banner'];
  ?>
<?php if ($banner): ?>
<div class="banner mb" style="background-image: url('<?= $banner ?>')"></div>
<?php endif; ?>
<section class="container">
  <div class="wrap">
    <div class="columns">
      <div class="content">
        <div class="title">
          <h1><?php the_title(); ?></h1>
        </div>
        <div class="editor">
          <?php the_content(); ?>
        </div>
        <div class="point-desc">
          <div class="info-list">
            <div class="row">
              <span><?= date('j ', $fields['start_date']).Chamberonne::getFRmonth(date('n', $fields['start_date']))
                      . date(' - j ', $fields['end_date']).Chamberonne::getFRmonth(date('n', $fields['end_date'])); ?></span>
            </div>
            <?php foreach ($fields['files'] as $file): ?>
            <div class="row">
              <a href="<?= $file['file']; ?>" class="doc-load-link" download><i class="icon icon-pdf"></i><?= $file['title']; ?></a>
            </div>
            <?php endforeach; ?>
            <div class="row">
              <div id="map-canvas" style="width: 100%; height: 400px;"></div>
            </div>
          </div>
          <a href="<?= get_post_type_archive_link(get_post_type()); ?>" class="btn"><?= $general_settings['all_traffic_info_button_title'] ?></a>
        </div>
      </div>
    </div>
  </div>
</section>
<script>
function gmap_initialize() {
  var latLng = new google.maps.LatLng(<?= $fields['location']['lat'] ?>, <?= $fields['location']['lng'] ?>);
  var mapOptions = {
    zoom: 14,
    center: latLng,
    scrollwheel: false
  };
  var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
  var mr_marker = new google.maps.Marker({
    position: latLng,
    map: map,
    draggable: false
  });
}
</script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=<?= $general_settings['google_api_key'] ?>&callback=gmap_initialize" async></script>
<?php
endwhile;

get_footer();
