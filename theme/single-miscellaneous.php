<?php
get_header();
$general_settings = Chamberonne::getGeneralSettings();
while (have_posts()):
  the_post();
  $fields = get_fields();
  $banner = $fields['banner']
                ?: $general_settings['single_miscellaneous_banner']
                ?: $general_settings['single_activity_banner']
                ?: $general_settings['activities_banner'];
  ?>
<?php if ($banner): ?>
<div class="banner mb" style="background-image: url('<?= $banner ?>')"></div>
<?php endif; ?>
<section class="container">
  <div class="wrap">
    <div class="columns">
      <div class="content">
        <div class="title">
          <h1><?php the_title(); ?></h1>
        </div>
        <div class="point-desc">
          <div class="info-list info-active">
            <?php Chamberonne::showActivityFields($fields); ?>
          </div>
          <a href="<?= get_post_type_archive_link(get_post_type()); ?>" class="btn"><?= $general_settings['all_miscellaneous_button_title'] ?></a>
        </div>
        <?php if (!empty($fields['images'])): ?>
        <section class="vehicles-posts">
          <div class="block-posts flex">
          <?php foreach ($fields['images'] as $image): ?>
            <a rel="gallery" href="<?= $image['image']; ?>" class="item-posts fancybox" >
              <div class="img" style="background-image: url('<?= $image['image']; ?>')"></div>
            </a>
          <?php endforeach; ?>
          </div>
        </section>
        <?php endif; ?>
      </div>
      <aside class="aside">
        <div class="cont">
          <div class="info">
            <div class="title">
              <h4>Alarmes <?= date('Y'); ?></h4>
            </div>
            <div class="info-alarms">
              <?php Chamberonne::yearAlarmsSummary(); ?>
            </div>
          </div>
        </div>
        <?php get_template_part('parts/next_activities'); ?>
      </aside>
    </div>
  </div>
</section>
<?php
endwhile;

get_footer();
