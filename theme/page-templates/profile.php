<?php
/*
 * Template Name: Profile
 */

$user = wp_get_current_user();
if (!$user->ID):
    wp_redirect(wp_login_url(get_permalink()));
    exit;
endif;

$general_settings = Chamberonne::getGeneralSettings();
get_header();

$role = ucfirst(reset($user->roles));
$agenda_page_id = Chamberonne::getPageIdByTemplate('page-templates/agenda.php');
$organization_page_id = Chamberonne::getPageIdByTemplate('page-templates/organization.php');

while (have_posts()):
the_post();
$fields = get_fields();
?>

<?php if (!empty($fields['banner'])): ?>
<div class="banner" style="background-image: url('<?= $fields['banner']; ?>')"></div>
<?php endif; ?>
<section class="section-person">
  <div class="wrap">
    <div class="info">
      <div class="title">
        <h1><?php the_title(); ?></h1>
      </div>
      <a href="<?= wp_logout_url(); ?>" class="btn"><?= $general_settings['logout_button_title']; ?></a>
    </div>
    <div class="person-area flex">
      <div class="item-area">
        <div class="title">
          <h2><?= $general_settings['profile_block_title']; ?></h2>
        </div>
        <a href="<?= get_edit_profile_url(); ?>" class="btn"><?= $general_settings['profile_block_button_title']; ?></a>
      </div>

      <?php if (in_array($role, $general_settings['agenda_block_show_for'])): ?>
      <div class="item-area">
        <div class="title">
          <h2><?= $general_settings['agenda_block_title']; ?></h2>
        </div>
        <a href="<?= get_permalink($agenda_page_id); ?>" class="btn"><?= $general_settings['agenda_block_button_title']; ?></a>
      </div>
      <?php endif; ?>

      <?php if (in_array($role, $general_settings['members_block_show_for'])): ?>
      <div class="item-area">
        <div class="title">
          <h2><?= $general_settings['members_block_title']; ?></h2>
        </div>
        <a href="<?= get_permalink($organization_page_id); ?>" class="btn"><?= $general_settings['members_block_button_title']; ?></a>
      </div>
      <?php endif; ?>

      <?php if (in_array($role, $general_settings['activities_block_show_for'])): ?>
      <div class="item-area">
        <div class="title">
          <h2><?= $general_settings['activities_block_title']; ?></h2>
        </div>
        <div class="list-alarms cont">
          <?php
          $query = new WP_Query([
            'post_type' => 'activity',
            'post_status' => 'publish',
            'posts_per_page' => 2,
            'meta_key' => 'date',
            'orderby' => 'meta_value',
            'order' => 'asc',
            'meta_query' => [
                [
                    'key' => 'date',
                    'value' => date('Y-m-d H:i:s'),
                    'compare' => '>'
                ],
            ],
          ]);
          while ($query->have_posts()):
            $query->the_post();
            $post_fields = get_fields();
            ?>
            <div class="row">
              <a href="<?php the_permalink(); ?>" class="text">
                <span class="title-bold"><?= date('d.m.y H\hi', $post_fields['date']); ?> <?php the_title(); ?></span>
                <?php if (!empty($post_fields['exercise_director'])): ?>
                <span>Directeur exercice: <?= $post_fields['exercise_director']; ?></span>
                <?php endif; ?>
                <?php if (!empty($post_fields['tenue'])): ?>
                <span>Tenue: <?= implode(', ', $post_fields['tenue']); ?></span>
                <?php endif; ?>
              </a>
            </div>
            <?php
            endwhile;
            wp_reset_postdata();
          ?>
        </div>
        <a href="<?= get_post_type_archive_link('activity'); ?>" class="btn"><?= $general_settings['activities_block_button_title']; ?></a>
      </div>
      <?php endif; ?>

      <?php
      // TODO: add Vehicles block
      ?>
      <?php if (in_array($role, $general_settings['vehicles_block_show_for'])): ?>
      <div class="item-area">
        <div class="title">
          <h2><?= $general_settings['vehicles_block_title']; ?></h2>
        </div>
        <div class="list-alarms cont">
          <?php
          $query = new WP_Query([
            'post_type' => 'vehicle',
            'post_status' => 'publish',
            'posts_per_page' => 2,
            'meta_key' => 'date',
            'orderby' => 'meta_value',
            'order' => 'asc',
            'meta_query' => [
                [
                    'key' => 'date',
                    'value' => date('Y-m-d H:i:s'),
                    'compare' => '>'
                ],
            ],
          ]);
          while ($query->have_posts()):
            $query->the_post();
            $post_fields = get_fields();
            $time_start = $post_fields['date'];
            $time_end = $post_fields['end_date'];
            if ($time_start && $time_end):
                $date_str = date('d.m.y', $time_start);
                if (date('d.m.y', $time_end) == $date_str):
                    $date_str .= ' de ' . date('H\hi', $time_start) . ' à ' . date('H\hi', $time_end);
                else:
                    $date_str = 'du ' . $date_str . date(' H\hi', $time_start) . ' au ' . date('d.m.y H\hi', $time_end);
                endif;
            else:
                $date_str = date('d.m.y H\hi', $time_start ?: $time_end);
            endif;
            ?>
            <div class="row">
              <a href="<?php the_permalink(); ?>" class="text">
                <span class="title-bold"><?= $date_str; ?></span>
                <?php if (!empty($post_fields['ressources'])): ?>
                <span><?= implode(', ', $post_fields['ressources']); ?></span>
                <?php endif; ?>
                <?php if (!empty($post_fields['description'])): ?>
                <span><?= $post_fields['description']; ?></span>
                <?php endif; ?>
              </a>
            </div>
            <?php
            endwhile;
            wp_reset_postdata();
          ?>
        </div>
        <a href="<?= get_post_type_archive_link('vehicle'); ?>" class="btn"><?= $general_settings['vehicles_block_button_title']; ?></a>
      </div>
      <?php endif; ?>

      <?php if (in_array($role, $general_settings['traffic_info_block_show_for'])): ?>
      <div class="item-area">
        <div class="title">
          <h2><?= $general_settings['traffic_info_block_title']; ?></h2>
        </div>
        <div class="cont">
          <?php
          $query = new WP_Query([
            'post_type' => 'traffic_info',
            'post_status' => 'publish',
            'posts_per_page' => 5,
            'meta_query' => [
                [
                    'key' => 'end_date',
                    'value' => date('Y-m-d H:i:s'),
                    'compare' => '>'
                ],
            ],
          ]);
          while ($query->have_posts()):
            $query->the_post();
            ?>
            <a href="<?php the_permalink(); ?>" class="link-item"><?php the_title(); ?></a>
            <?php
            endwhile;
            wp_reset_postdata();
          ?>
        </div>
        <a href="<?= get_post_type_archive_link('traffic_info'); ?>" class="btn"><?= $general_settings['traffic_info_block_button_title']; ?></a>
      </div>
      <?php endif; ?>

      <?php if (in_array($role, $general_settings['documents_block_show_for'])): ?>
      <div class="item-area">
        <div class="title">
          <h2><?= $general_settings['documents_block_title']; ?></h2>
        </div>
        <div class="cont">
          <?php
          $query = new WP_Query([
            'post_type' => 'document',
            'post_status' => 'publish',
            'posts_per_page' => 5,
          ]);
          while ($query->have_posts()):
            $query->the_post();
            $post_fields = get_fields();
            ?>
            <a href="<?= $post_fields['file']; ?>" class="link-item" download><?php the_title(); ?></a>
            <?php
            endwhile;
            wp_reset_postdata();
          ?>
        </div>
        <a href="<?= get_post_type_archive_link('document'); ?>" class="btn"><?= $general_settings['documents_block_button_title']; ?></a>
      </div>
      <?php endif; ?>
    </div>
  </div>
</section>

<?php
endwhile;

get_footer();
