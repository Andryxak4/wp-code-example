<?php
/*
 * Template Name: Contact
 */

get_header();
$general_settings = Chamberonne::getGeneralSettings();
while (have_posts()):
    the_post();
    $fields = get_fields();
?>

<?php if (!empty($fields['banner'])): ?>
<div class="banner mb" style="background-image: url('<?= $fields['banner'] ?>')"></div>
<?php endif; ?>
  
<section class="container contact-page">
  <div class="wrap">
    <div class="columns">
      <section class="content">
        <div class="title">
          <h1><?php the_title(); ?></h1>
        </div>
        <div class="contacts">
          <?php foreach ($fields['contacts'] as $item): ?>
          <div class="contact-item">
            <?= $item['content']; ?>
          </div>
          <?php endforeach; ?>
        </div>
        <div id="map-canvas" style="width: 100%; height: 400px;"></div>
      </section>
      <aside class="aside">
        <div class="cont">
          <div class="info">
            <div class="title">
              <h4><?= $fields['right_block_title']; ?></h4>
            </div>
            <div class="right-content">
              <?= $fields['right_block_content']; ?>
            </div>
            <div class="contact-email">
              <a href="mailto:<?= $general_settings['contact_email']; ?>"><?= $general_settings['contact_email']; ?></a>
            </div>
          </div>
        </div>
      </aside>
    </div>
  </div>
</section>

<?php if ($fields['contacts']): ?>
<script>
function gmap_initialize() {
  var mapOptions = {
    zoom: 14,
    scrollwheel: false
  };
  var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
  
  var contacts = <?= json_encode($fields['contacts']); ?>;
  var bounds = new google.maps.LatLngBounds();

  var openedInfoWindow;
  contacts.forEach(function(item) {
    var latLng = new google.maps.LatLng(item.location.lat, item.location.lng);
    var infoWindow = new google.maps.InfoWindow({
      content: item.content
    });
    var marker = new google.maps.Marker({
      position: latLng,
      map: map,
      draggable: false
    });
    marker.addListener('click', function() {
      openedInfoWindow && openedInfoWindow.close();
      infoWindow.open(map, this);
      openedInfoWindow = infoWindow;
    });
    bounds.extend(marker.position);
  });
  map.fitBounds(bounds);
}
</script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=<?= $general_settings['google_api_key'] ?>&callback=gmap_initialize" async></script>
<?php
endif;
endwhile;
get_footer();
