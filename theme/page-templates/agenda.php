<?php
/*
 * Template Name: Agenda
 */

get_header();

while (have_posts()):
    the_post();
    $fields = get_fields();
?>

<section class="container agenda-page">
  <div class="wrap">
    <section class="section-text single-text">
      <div class="title">
        <h1><?php the_title(); ?></h1>
      </div>
      <div class="editor">
        <?php the_content(); ?>
      </div>
      <div class="action-btn calendar-filters">
        <a href="#" class="btn active" data-filter="alarms">Alarmes</a>
        <a href="#" class="btn active" data-filter="activities">Activités</a>
        <a href="#" class="btn active" data-filter="miscellaneous">Divers</a>
        <a href="#" class="btn" data-filter="officers">Officier de piquet</a>
        <a href="#" class="btn active" data-filter="vehicles">Ressources</a>
      </div>
    </section>
    <div id='calendar'></div>
  </div>
</section>

<div style="display:none;">
  <div id="calendar-export">
    <h3><?= $fields['export_modal_title']; ?></h3>
    <form action="#">
      <div class="form-checkbox-wrapper">
        <div class="form-checkbox">
          <label>
            <input type="checkbox" name="alarm">
            <span>Alarmes</span>
          </label>
        </div>
        <div class="form-checkbox">
          <label>
            <input type="checkbox" name="activity">
            <span>Activités</span>
          </label>
        </div>
        <div class="form-checkbox">
          <label>
            <input type="checkbox" name="miscellaneous">
            <span>Divers</span>
          </label>
        </div>
        <div class="form-checkbox">
          <label>
            <input type="checkbox" name="officer">
            <span>Officier de piquet</span>
          </label>
        </div>
        <div class="form-checkbox">
          <label>
            <input type="checkbox" name="vehicle">
            <span>Ressources</span>
          </label>
        </div>
      </div>
      <div class="buttons">
        <button type="submit" class="btn"><?= $fields['export_modal_button']; ?></button>
      </div>
      <div class="success"><?= $fields['export_modal_success_message']; ?></div>
    </form>
  </div>
</div>

<?php
endwhile;
get_footer();

