<?php
/*
 * Template Name: Organization
 */

$user_id = get_current_user_id();

get_header();

while (have_posts()):
  the_post();
  $fields = get_fields();
?>

<?php if (!empty($fields['banner'])): ?>
  <div class="banner" style="background-image: url('<?= $fields['banner'] ?>')"></div>
  <?php endif; ?>
  <section class="container vehicles-section organization">
    <div class="wrap">
      <section class="section-text single-text">
        <div class="title">
          <h1><?php the_title(); ?></h1>
        </div>
        <div class="editor">
          <?php the_content(); ?>
        </div>
      </section>
    </div>
  </section>
  
  <?php
  $query = new WP_Query([
      'post_type' => 'member',
      'post_status' => 'publish',
      'posts_per_page' => -1,
      'meta_query' => [
          'relation' => 'OR',
          [
              'key' => 'hide_for_users',
              'value' => '"' . $user_id . '"',
              'compare' => 'NOT LIKE',
          ],
          [
              'key' => 'hide_for_users',
              'compare' => 'NOT EXISTS',
          ]
      ]
  ]);
  ?>
  <section class="vehicles-posts organization">
    <div class="wrap">
      <div class="block-posts flex">
      <?php
      while ($query->have_posts()):
        $query->the_post();
        $thumb_id = get_post_thumbnail_id();
        $thumb_url = wp_get_attachment_image_url($thumb_id, 'medium');
        ?>
        <div class="item-posts">
          <?php if ($thumb_url): ?>
          <div class="img" style="background-image: url('<?= $thumb_url ?>')"></div>
          <?php endif; ?>
          <div class="member-description-main">
            <span class="title-post"><?php the_title(); ?></span>
            <?php Chamberonne::showMemberFields($fields, 'list'); ?>
          </div>
        </div>
      <?php
      endwhile;
      wp_reset_postdata();
      ?>
      </div>
    </div>
  </section>

<?php
endwhile;

get_footer();
