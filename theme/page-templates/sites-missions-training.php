<?php
/*
 * Template Name: Sites, Missions, Training
 */

get_header();

while (have_posts()):
  the_post();
  $fields = get_fields();
?>

<?php if (!empty($fields['banner'])): ?>
<div class="banner" style="background-image: url('<?= $fields['banner'] ?>')"></div>
<?php endif; ?>

<section class="container ">
  <div class="wrap">
    <section class="section-text single-text">
      <div class="title">
        <h1><?php the_title(); ?></h1>
      </div>
      <div class="editor">
        <?php the_content(); ?>
      </div>
    </section>
  </div>
</section>

<?php if (!empty($fields['parallax_image'])): ?>
<div class="banner-parallax" style="background-image: url('<?= $fields['parallax_image'] ?>')"></div>
<?php endif; ?>

<?php if (!empty($fields['bottom_sections'])): ?>
<section class="container">
  <div class="wrap">
    <div class="section-text twin-text flex">
      <?php foreach ($fields['bottom_sections'] as $section): ?>
      <div class="item-text">
        <div class="title">
          <h1><?= $section['title']; ?></h1>
        </div>
        <div class="editor">
          <p><?= $section['content']; ?></p>
        </div>
      </div>
      <?php endforeach; ?>
    </div>
  </div>
</section>
<?php endif; ?>

<?php if (!empty($fields['links'])): ?>
<section class="regulation-documents">
  <div class="wrap">
    <?php if (!empty($fields['links_section_title'])): ?>
    <div class="title">
      <h4><?= $fields['links_section_title']; ?></h4>
    </div>
    <?php endif; ?>
    <div class="block-btn flex">
      <?php foreach ($fields['links'] as $link): ?>
      <a href="<?= $link[$link['file_or_url']]; ?>" class="btn" download><?= $link['title']; ?></a>
      <?php endforeach; ?>
    </div>
  </div>
</section>
<?php endif; ?>

<?php
endwhile;

get_footer();
