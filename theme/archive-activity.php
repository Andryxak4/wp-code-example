<?php
get_header();
$year = Functions::arrayItem($_GET, 'y', date('Y'));
$general_settings = Chamberonne::getGeneralSettings();
$is_activities_archive = is_post_type_archive('activity');
$is_miscellaneous_archive = is_post_type_archive('miscellaneous');
$activities_misc_data = Chamberonne::getYearActivitiesAndMiscData($year);
$activities_years = Chamberonne::getDatedPostsYears('activity');

$activities_html = $misc_html = '';
$activity_last_month = 0;
$misc_last_month = 0;
foreach ($activities_misc_data as $row):
    $timestamp = strtotime($row['date']);
    $month = date('n', $timestamp);
    $item = '
                <a href="'. get_permalink($row['ID']) .'" class="row">
                  <span class="name">'.$row['post_title'].'</span>
                  <span class="desc">'.$row['description'].'</span>
                  <span class="country">'.$row['location'].'</span>
                  <div class="datetime">
                    '.Chamberonne::getFRdayOfWeek(date('w', $timestamp)).date(' d.m, H:i', $timestamp).'
                  </div>'
                . ($row['images_count'] ? '<i class="icon icon-picture"></i>' : '') . '
                </a>';
    if ($row['post_type'] == 'activity'):
        if ($month != $activity_last_month):
            if ($activity_last_month):
                $activities_html .= "\n</div>\n"; // close previous month's .block-list
            endif;
            $activity_last_month = $month;
            $activities_html .= '<div class="title"><h2>' . Chamberonne::getFRmonth($month) . "</h2></div>\n";
            $activities_html .= '<div class="block-list">';
        endif;
        $activities_html .= $item;
    else:
        if ($month != $misc_last_month):
            if ($misc_last_month):
                $misc_html .= "\n</div>\n"; // close previous month's .block-list
            endif;
            $misc_last_month = $month;
            $misc_html .= '<div class="title"><h2>' . Chamberonne::getFRmonth($month) . "</h2></div>\n";
            $misc_html .= '<div class="block-list">';
        endif;
        $misc_html .= $item;
    endif;
endforeach;
if ($activities_html) $activities_html .= "\n</div>\n"; // close the last .block-list
if ($misc_html) $misc_html .= "\n</div>\n";             // close the last .block-list
?>
  <?php if (!empty($general_settings['activities_banner'])): ?>
  <div class="banner mb" style="background-image: url('<?= $general_settings['activities_banner'] ?>')"></div>
  <?php endif; ?>
  <section class="container">
    <div class="wrap">
      <div class="columns">
        <div class="content">
          <div class="title">
            <h1><?= $general_settings['activities_title'] ?></h1>
          </div>
          <div class="block-tabs">
            <div class="links-tab cart-tabs">
              <a href="#tab-activities" class="link btn<?= $is_activities_archive ? ' active' : '' ?>">Activités</a>
              <a href="#tab-miscellaneous" class="link btn<?= $is_miscellaneous_archive ? ' active' : '' ?>">Divers</a>
            </div>
            <div class="activities table-list wrap-tabs">
              <div class="cart-tabs__item<?= $is_activities_archive ? ' cart-tabs__item--active' : '' ?>"
                   id="tab-activities"<?= $is_activities_archive ? '' : ' style="display: none;"' ?>>
                <?= $activities_html; ?>
              </div>
              <div class="cart-tabs__item<?= $is_miscellaneous_archive ? ' cart-tabs__item--active' : '' ?>"
                   id="tab-miscellaneous"<?= $is_miscellaneous_archive ? '' : ' style="display: none;"' ?>>
                <?= $misc_html; ?>
              </div>
            </div>
          </div>
        </div>
        <aside class="aside">
          <?php get_template_part('parts/next_activities'); ?>
          <?php if ($activities_years && (count($activities_years)>1 || $activities_years[0]!=$year)): ?>
          <div class="block-archives">
            <div class="title">
              <h4>Archives</h4>
            </div>
            <ul>
              <?php foreach ($activities_years as $val): ?>
              <?php if ($val != $year): ?>
              <li><a href="<?= home_url('/download-activities?y='.$val) ?>">Activités <?= $val ?></a></li>
              <?php endif; ?>
              <?php endforeach; ?>
            </ul>
          </div>
          <?php endif; ?>
        </aside>
      </div>
    </div>
  </section>
<?php
get_footer();
