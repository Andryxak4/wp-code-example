<?php
get_header();

while (have_posts()):
    the_post();
?>

<section class="container">
  <div class="wrap">
    <section class="section-text single-text">
      <div class="title">
        <h1><?php the_title(); ?></h1>
      </div>
      <div class="editor">
        <?php the_content(); ?>
      </div>
    </section>
  </div>
</section>

<?php
endwhile;
get_footer();
