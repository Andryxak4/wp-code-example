<?php
get_header();
$fields = get_fields();
?>
    <?php if (!empty($fields['slider'])): ?>
    <section class="section-main">
      <div class="slider-main">
        <div class="swiper-container big-sl">
          <div class="swiper-wrapper">
            <?php foreach ($fields['slider'] as $slide): ?>
            <div class="swiper-slide">
              <div class="wrap">
                <div class="info">
                  <div class="title">
                    <h1><?= $slide['title'] ?></h1>
                  </div>
                  <div class="editor">
                    <p><?= $slide['description']; ?></p>
                  </div>
                  <div class="action flex">
                    <a href="<?= $slide['button_url']; ?>" class="btn"><?= $slide['button_title']; ?></a>
                    <div class="sl-btn">
                      <div class="big-sl-prev swiper-button-prev swiper-button-black"></div>
                      <div class="big-sl-next swiper-button-next swiper-button-black"></div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="img" style="background-image: url('<?= $slide['image']; ?>')"></div>
            </div>
            <?php endforeach; ?>
          </div>
          <div class="big-sl-pagination swiper-pagination"></div>
        </div>
      </div>
    </section>
    <?php endif; ?>
    <section class="main-info">
      <div class="wrap">
        <div class="columns">
          <div class="content">
            <div class="block-main">
              <div class="title">
                <h2><?= $fields['left_section_title'] ?></h2>
              </div>
              <div class="editor mb-15">
                <p><?= $fields['left_section_content'] ?></p>
              </div>
              <a href="<?= $fields['left_section_button_url'] ?>" class="btn"><?= $fields['left_section_button_title'] ?></a>
            </div>
          </div>
          <aside class="aside">
            <?php get_template_part('parts/next_activities'); ?>
            <?php get_template_part('parts/last_alarms'); ?>
          </aside>
        </div>
      </div>
    </section>
<?php
get_footer();
