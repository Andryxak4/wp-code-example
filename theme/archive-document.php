<?php
get_header();
$general_settings = Chamberonne::getGeneralSettings();
$profile_page_id = Chamberonne::getPageIdByTemplate('page-templates/profile.php');
$dashboard_btn_url = get_permalink($profile_page_id);
$documents_data = Chamberonne::getDocumentsData();
$documents_by_category = [];
foreach ($documents_data as $item):
    $documents_by_category[$item['category']][] = [
        'title' => $item['title'],
        'link' => $item['link'],
    ];
endforeach;
?>
<?php if (!empty($general_settings['documents_banner'])): ?>
<div class="banner" style="background-image: url('<?= $general_settings['documents_banner'] ?>')"></div>
<?php endif; ?>

<section class="section-docs">
  <div class="wrap">
    <div class="info">
      <div class="title">
        <h1><?= $general_settings['documents_title']; ?></h1>
      </div>
      <div class="action">
        <a href="<?= $dashboard_btn_url; ?>" class="btn"><?= $general_settings['dashboard_button_title']; ?></a>
        <a href="<?= wp_logout_url(); ?>" class="btn"><?= $general_settings['logout_button_title']; ?></a>
      </div>
    </div>
    <div class="block-docs flex">
      <?php foreach ($documents_by_category as $category => $docs): ?>
      <div class="item">
        <div class="title">
          <h2><?= $category; ?></h2>
        </div>
        <div class="list-doc">
          <?php foreach ($docs as $document): ?>
          <a href="<?= $document['link'] ?>" class="doc-load-link" download><i class="icon icon-pdf"></i><?= $document['title']; ?></a>
          <?php endforeach; ?>
        </div>
      </div>
      <?php endforeach; ?>
    </div>
  </div>
</section>

<?php
get_footer();
