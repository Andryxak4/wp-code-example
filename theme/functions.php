<?php

require_once __DIR__ . '/includes/functions-class.php';
require_once __DIR__ . '/includes/chamberonne-class.php';
require_once __DIR__ . '/includes/ajax.php';

add_action('after_setup_theme', function() {
    add_theme_support('post-thumbnails');
    register_nav_menu('primary', __('Main menu', 'chamberonne'));
});

acf_add_options_page([
    'page_title' => __('Theme settings', 'chamberonne'),
    'menu_title' => __('Theme settings', 'chamberonne'),
    'menu_slug' => 'theme-settings',
]);
acf_add_options_sub_page([
    'page_title' => __('Theme settings', 'chamberonne'),
    'menu_title' => __('Theme settings', 'chamberonne'),
    'menu_slug' => 'acf-options-theme-settings',
    'parent_slug' => 'theme-settings',
]);
acf_add_options_sub_page([
    'page_title' => __('Member\'s Area', 'chamberonne'),
    'menu_title' => __('Member\'s Area', 'chamberonne'),
    'menu_slug' => 'acf-options-members-area',
    'parent_slug' => 'theme-settings',
]);
acf_add_options_sub_page([
    'page_title' => __('APIs', 'chamberonne'),
    'menu_title' => __('APIs', 'chamberonne'),
    'menu_slug' => 'acf-options-apis',
    'parent_slug' => 'theme-settings',
]);

add_action("after_switch_theme", function($stylesheet, $old_theme) {
    Chamberonne::setUserRoles();
}, 10, 2);


// == STYLES & SCRIPTS ==

remove_action('wp_head', 'print_emoji_detection_script', 7);

add_action('wp_enqueue_scripts', function() {
    wp_dequeue_style('wp-block-library');
    wp_enqueue_style('theme-all-styles', get_template_directory_uri().'/assets/css/all.css', [], filemtime(get_template_directory().'/assets/css/all.css'));

    if (!is_admin()) {
        wp_deregister_script('jquery');
        wp_deregister_script('wp-embed');
    }
    wp_enqueue_script('jquery', get_template_directory_uri().'/assets/js/jquery.min.js', [], false, true);
    if (is_front_page()) {
        wp_enqueue_script('theme-js-swiper', get_template_directory_uri().'/assets/js/swiper.js', ['jquery'], false, true);
        wp_enqueue_script('theme-js-function_slider', get_template_directory_uri().'/assets/js/function_slider.js', ['jquery', 'theme-js-swiper'], false, true);
    }
    if (is_page_template('page-templates/agenda.php')) {
        wp_enqueue_script('theme-js-fullcalendar-core', get_template_directory_uri().'/assets/js/fullcalendar-core.js', [], false, true);
        wp_enqueue_script('theme-js-fullcalendar-locale-fr', get_template_directory_uri().'/assets/js/fullcalendar-locale-fr.js', ['theme-js-fullcalendar-core'], false, true);
        wp_enqueue_script('theme-js-fullcalendar-interaction', get_template_directory_uri().'/assets/js/fullcalendar-interaction.js', [], false, true);
        wp_enqueue_script('theme-js-fullcalendar-daygrid', get_template_directory_uri().'/assets/js/fullcalendar-daygrid.js', [], false, true);
        wp_enqueue_script('theme-js-calendar', get_template_directory_uri().'/assets/js/profile-calendar.js', ['jquery'], false, true);
    }
    if (is_singular(['alarm', 'activity', 'miscellaneous']) || is_page_template('page-templates/agenda.php')) {
        wp_enqueue_script('theme-js-jquery.fancybox', get_template_directory_uri().'/assets/js/jquery.fancybox.js', ['jquery'], false, true);
    }
    wp_enqueue_script('theme-js-main', get_template_directory_uri().'/assets/js/main.js', ['jquery'], filemtime(get_template_directory().'/assets/js/main.js'), true);
    wp_localize_script('theme-js-main', 'chamberonne', [
        'ajaxurl' => admin_url('admin-ajax.php'),
        'google_api_key' => get_field('google_api_key', 'options'),
        'google_oauth_client_id' => get_field('google_oauth_client_id', 'options'),
        'outlook_azure_ad_application_id' => get_field('outlook_azure_ad_application_id', 'options'),
    ]);
});

add_action('admin_menu', function() {
    add_submenu_page('tools.php', 'SDIS Chamberonne Activities Import', 'SDIS Chamberonne Activities Import', 'manage_options', 'admin.php?import=sdis-activities-importer');
    register_importer('sdis-activities-importer', 'SDIS Chamberonne Activities Import', 'Import activities data from CSV', [Chamberonne::class, 'importActivitiesCSV']);
});


add_action('init', function() {
    // == POST TYPES ==
    Functions::registerPostType('vehicle', [
        'args' => [
            'supports' => ['title', 'thumbnail'],
            'rewrite' => [
                'slug' => 'vehicules',
                'with_front' => false,
            ],
            'capability_type' => 'vehicle',
        ],
    ]);
    Functions::registerPostType('member', [
        'args' => [
            'supports' => ['title', 'thumbnail'],
            'rewrite' => [
                'slug' => 'membres',
                'with_front' => false,
            ],
            'capability_type' => 'member',
        ],
    ]);
    Functions::registerPostType('alarm', [
        'args' => [
            'supports' => ['title'],
            'rewrite' => [
                'slug' => 'alarmes',
                'with_front' => false,
            ],
            'capability_type' => 'alarm',
        ],
    ]);
    Functions::registerPostType('activity', [
        'plural' => 'activities',
        'args' => [
            'supports' => ['title'],
            'rewrite' => [
                'slug' => 'activites',
                'with_front' => false,
            ],
            'capability_type' => ['activity', 'activities'],
        ],
    ]);
    Functions::registerPostType('miscellaneous', [
        'plural' => 'miscellaneous',
        'args' => [
            'supports' => ['title'],
            'rewrite' => [
                'slug' => 'divers',
                'with_front' => false,
            ],
            'capability_type' => ['miscellaneous', 'miscellaneous'],
        ],
    ]);
    Functions::registerPostType('traffic info', [
        'plural' => 'traffic info',
        'args' => [
            'rewrite' => [
                'slug' => 'traffic-info',
                'with_front' => false,
            ],
            'capability_type' => ['traffic_info', 'traffic_info'],
        ],
    ]);
    Functions::registerPostType('document', [
        'args' => [
            'supports' => ['title'],
            'rewrite' => [
                'slug' => 'documents',
                'with_front' => false,
            ],
            'capability_type' => 'document',
        ],
    ]);
    Functions::registerPostType('officer', [
        'args' => [
            'supports' => ['title'],
            'rewrite' => [
                'slug' => 'officiers',
                'with_front' => false,
            ],
            'capability_type' => 'officer',
        ],
    ]);

    // == TAXONOMIES ==
    Functions::registerTaxonomy('alarm type', 'alarm', [
        'args' => [
            'show_admin_column' => true,
            'rewrite' => ['slug' => 'alarm-type'],
            'capabilities' => [
                'assign_terms' => 'edit_alarms',
            ],
        ],
    ]);
    Functions::registerTaxonomy('barracks', ['vehicle', 'activity', 'miscellaneous'], [
        'plural' => 'barracks',
        'args' => [
            'show_admin_column' => true,
            'capabilities' => [
                'assign_terms' => 'assign_barracks',
            ],
        ]
    ]);
    Functions::registerTaxonomy('location', ['alarm', 'activity', 'miscellaneous'], [
        'args' => [
            'show_admin_column' => true,
            'capabilities' => [
                'assign_terms' => 'assign_location',
            ],
        ]
    ]);
    Functions::registerTaxonomy('document category', 'document', [
        'plural' => 'document categories',
        'args' => [
            'show_admin_column' => true,
            'capabilities' => [
                'assign_terms' => 'edit_documents',
            ],
        ],
    ]);
});

// == ALLOW TO UPLOAD SVG ==

add_filter('upload_mimes', function($mimes) {
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
});

add_filter('wp_check_filetype_and_ext', function($data, $file, $filename, $mimes) {
    $filetype = wp_check_filetype($filename, $mimes);
    return [
        'ext' => $filetype['ext'],
        'type' => $filetype['type'],
        'proper_filename' => $data['proper_filename']
    ];
}, 10, 4);


// == SET DEFAULT ALARM NUMBER VALUE ==
add_filter('acf/load_field/name=alarm_number', function($field) {
    $next_number = Chamberonne::getNextAlarmNumber();
    $field['default_value'] = $next_number;
    return $field;
});


// == DOWNLOAD ALARMS ==
add_action('template_redirect', function() {
    if (stripos($_SERVER['REQUEST_URI'], '/download-alarms?y=') !== false
            && !empty($_GET['y']) && is_numeric($_GET['y'])):
        http_response_code(200);
        Chamberonne::downloadAlarmArchive($_GET['y']);
    elseif (stripos($_SERVER['REQUEST_URI'], '/download-activities?y=') !== false
            && !empty($_GET['y']) && is_numeric($_GET['y'])):
        http_response_code(200);
        Chamberonne::downloadActivityArchive($_GET['y']);
    elseif (stripos($_SERVER['REQUEST_URI'], '/sdis-calendar-events?filter[]=') !== false
            && !empty($_GET['filter'])):
        http_response_code(200);
        Chamberonne::exportCalendarEvents($_GET['y']);
    endif;
});


// == ACF GOOGLE MAP KEY ==
add_filter('acf/fields/google_map/api', function($api) {
    $api['key'] = get_field('google_api_key', 'options');
    return $api;
});


// == CLEAR CACHE ON UPDATE ACF OPTIONS ==
add_action('acf/save_post', function() {
    $screen = get_current_screen();
    if ((strpos($screen->id, "acf-options-theme-settings") !== false)
            || (strpos($screen->id, "acf-options-members-area") !== false)
            || (strpos($screen->id, "acf-options-apis") !== false)
            ) {
        delete_transient('chamberonne-theme-options');
    }
}, 20);
