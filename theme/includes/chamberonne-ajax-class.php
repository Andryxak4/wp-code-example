<?php

class ChamberonneAjax
{

    public static $_dateFormat = 'c';

    public static function getEventsByType($post_type, $start, $end, $fields = ['title', 'url', 'start', 'end'])
    {
        /* @var $timezone DateTimeZone */
        $timezone = wp_timezone();
        $args = [
            'post_type' => $post_type,
            'posts_per_page' => -1,
            'post_status' => 'publish',
//            'meta_key' => 'date',
//            'orderby' => 'meta_value',
//            'order' => 'desc',
            'meta_query' => [
              [
                'relation' => 'OR',
                [
                    'key' => 'date',
                    'value' => date('Y-m-d H:i:s', $start),
                    'compare' => '>'
                ],
                [
                    'key' => 'end_date',
                    'value' => date('Y-m-d H:i:s', $start),
                    'compare' => '>'
                ]
              ]
            ],
        ];
        if ($end):
            $args['meta_query'][] = [
              'relation' => 'OR',
              [
                'key' => 'date',
                'value' => date('Y-m-d H:i:s', $end),
                'compare' => '<'
              ],
              [
                'key' => 'end_date',
                'value' => date('Y-m-d H:i:s', $end),
                'compare' => '<'
              ]
            ];
        endif;
        $query = new WP_Query($args);
        
        $result = [];
        while ($query->have_posts()):
            $query->the_post();
            $post = get_post();
            $date = get_post_meta($post->ID, 'date', true);
            $end_date = get_post_meta($post->ID, 'end_date', true);
            $datetime = new DateTime($date, $timezone);
            $end_datetime = new DateTime($end_date, $timezone);
            $item = [];
            if (in_array('id', $fields)):
                $item['id'] = $post->ID;
            endif;
            if (in_array('title', $fields)):
                $item['title'] = $post->post_title;
            endif;
            if (in_array('start', $fields)):
                $item['start'] = $datetime->format(static::$_dateFormat);
            endif;
            if (in_array('end', $fields)):
                $item['end'] = $end_datetime->format(static::$_dateFormat);
            endif;
            if (in_array('url', $fields)):
                $item['url'] = get_permalink();
            endif;
            if (in_array('type', $fields)):
                $item['type'] = get_post_type();
            endif;
            $result[] = $item;
        endwhile;
        wp_reset_postdata();
        return $result;
    }

    public static function getCalendarEventsAlarms()
    {
        $start = strtotime($_GET['start']);
        $end = strtotime($_GET['end']);
        $result = self::getEventsByType('alarm', $start, $end);
        exit(json_encode($result));
    }

    public static function getCalendarEventsActivities()
    {
        $start = strtotime($_GET['start']);
        $end = strtotime($_GET['end']);
        $result = self::getEventsByType('activity', $start, $end);
        exit(json_encode($result));
    }

    public static function getCalendarEventsMiscellaneous()
    {
        $start = strtotime($_GET['start']);
        $end = strtotime($_GET['end']);
        $result = self::getEventsByType('miscellaneous', $start, $end);
        exit(json_encode($result));
    }

    public static function getCalendarEventsOfficers()
    {
        $start = strtotime($_GET['start']);
        $end = strtotime($_GET['end']);
        $result = self::getEventsByType('officer', $start, $end, ['title', 'start', 'end']);
        exit(json_encode($result));
    }

    public static function getCalendarEventsVehicles()
    {
        $start = strtotime($_GET['start']);
        $end = strtotime($_GET['end']);
        $result = self::getEventsByType('vehicle', $start, $end);
        exit(json_encode($result));
    }

}
