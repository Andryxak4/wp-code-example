<?php
$data = self::getYearAlarmsData($year);
?>
<body>
<style>
h2 {
  text-transform: uppercase;
}
table {
  width: 100%;
  border-spacing: 0;
  font-family: Arial;
}
table, td {
  border-top: 1px solid #E5E5E5;
  border-bottom: 1px solid #E5E5E5;
}
td {
  padding: 5px;
}
</style>
<h1>Alarmes <?= $year; ?></h1>
<?php
$last_month = 0;
foreach ($data as $row):
  $timestamp = strtotime($row['date']);
  $month = date('n', $timestamp);
  if ($month != $last_month):
    if ($last_month):
      echo '</tbody></table>';
    endif;
    $last_month = $month;
    ?>
    <h2><?= Chamberonne::getFRmonth($month) ?></h2>
    <table>
      <tbody>
  <?php endif; ?>
      <tr>
        <td style="width: 6%; color: #EB061E;"><?= $row['number'] ?></td>
        <td style="width: 16%; color: #EB061E;"><?= $row['post_title'] ?></td>
        <td style="width: 32%"><?= $row['description'] ?></td>
        <td style="width: 16%"><?= $row['location'] ?></td>
        <td style="width: 14%"><?= Chamberonne::getFRdayOfWeek(date('w', $timestamp)) ?></td>
        <td style="width: 8%"><?= date('d.m', $timestamp) ?></td>
        <td style="width: 8%"><?= date('H:i', $timestamp) ?></td>
      </tr>
<?php endforeach; ?>
  </tbody>
</table>

</body>
