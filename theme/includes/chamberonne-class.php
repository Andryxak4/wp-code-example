<?php

class Chamberonne
{

    private static $_alarmsData = [];
    private static $_activitiesAndMiscData = [];

    public static function getGeneralSettings()
    {
        $transient_key = 'chamberonne-theme-options';
        $generalSettings = get_transient($transient_key);
        if ($generalSettings === false):
            $generalSettings = get_fields('options');
            set_transient($transient_key, $generalSettings, 900);
        endif;
        return $generalSettings;
    }

    public static function getPageIdByTemplate($template)
    {
        $transient_key = 'chamberonne-page-template-'.$template;
        $result = get_transient($transient_key);
        if ($result === false):
            $args = [
                'post_type' => 'page',
                'fields' => 'ids',
                'nopaging' => true,
                'meta_key' => '_wp_page_template',
                'meta_value' => $template
            ];
            $pages = get_posts( $args );
            $result = Functions::arrayItem($pages, 0);
            set_transient($transient_key, $result, 900);
        endif;
        
        return $result;
    }

    public static function setUserRoles()
    {
        $admin_role = get_role('administrator');
        $admin_role->add_cap('assign_barracks');
        $admin_role->add_cap('assign_location');

        $post_types_caps = [
            ['alarm', 'alarms'],
            ['activity', 'activities'],
            ['traffic_info', 'traffic_info'],
            ['miscellaneous', 'miscellaneous'],
            ['member', 'members'],
            ['vehicle', 'vehicles'],
            ['document', 'documents'],
            ['officer', 'officers'],
        ];
        foreach ($post_types_caps as $cap):
            $admin_role->add_cap('read_'.$cap[0]);
            $admin_role->add_cap('edit_'.$cap[0]);
            $admin_role->add_cap('delete_'.$cap[0]);
            $admin_role->add_cap('edit_'.$cap[1]);
            $admin_role->add_cap('delete_'.$cap[1]);
            $admin_role->add_cap('edit_others_'.$cap[1]);
            $admin_role->add_cap('publish_'.$cap[1]);
            $admin_role->add_cap('read_private_'.$cap[1]);
        endforeach;

        remove_role('member');
        remove_role('technical');
        remove_role('trainer');
        remove_role('moderator');
        add_role('member', __('Member', 'chamberonne'), [
            'read' => true,
            'upload_files' => true,

            'read_alarm' => true,
            'read_activity' => true,
            'read_traffic_info' => true,
            'read_member' => true,
            'read_vehicle' => true,
            'read_document' => true,
            'read_miscellaneous' => true,
            'read_page' => true,

            'edit_alarm' => true,
            'delete_alarm' => true,
            'edit_alarms' => true,
            'delete_alarms' => true,
            'assign_location' => true,

            'edit_activity' => true,
            'delete_activity' => true,
            'edit_activities' => true,
            'delete_activities' => true,
            'assign_barracks' => true,
        ]);
        add_role('technical', __('Technical', 'chamberonne'), [
            'read' => true,
            'upload_files' => true,

            'read_alarm' => true,
            'read_activity' => true,
            'read_traffic_info' => true,
            'read_member' => true,
            'read_vehicle' => true,
            'read_document' => true,
            'read_miscellaneous' => true,
            'read_page' => true,

            'edit_vehicle' => true,
            'delete_vehicle' => true,
            'edit_vehicles' => true,
            'delete_vehicles' => true,
            'edit_others_vehicles' => true,
            'publish_vehicles' => true,
            'read_private_vehicles' => true,
            'assign_barracks' => true,
        ]);
        add_role('trainer', __('Trainer', 'chamberonne'), [
            'read' => true,
            'upload_files' => true,

            'read_alarm' => true,
            'read_activity' => true,
            'read_traffic_info' => true,
            'read_member' => true,
            'read_vehicle' => true,
            'read_document' => true,
            'read_miscellaneous' => true,
            'read_page' => true,

            'edit_alarm' => true,
            'delete_alarm' => true,
            'edit_alarms' => true,
            'delete_alarms' => true,
            'edit_others_alarms' => true,
            'publish_alarms' => true,
            'read_private_alarms' => true,
            'assign_location' => true,

            'edit_vehicle' => true,
            'delete_vehicle' => true,
            'edit_vehicles' => true,
            'delete_vehicles' => true,
            'edit_others_vehicles' => true,
            'publish_vehicles' => true,
            'read_private_vehicles' => true,
            'assign_barracks' => true,
        ]);
        add_role('moderator', __('Moderator', 'chamberonne'), [
            'read' => true,
            'upload_files' => true,

            'read_alarm' => true,
            'read_activity' => true,
            'read_traffic_info' => true,
            'read_member' => true,
            'read_vehicle' => true,
            'read_document' => true,
            'read_miscellaneous' => true,
            'read_page' => true,

            'edit_alarm' => true,
            'delete_alarm' => true,
            'edit_alarms' => true,
            'delete_alarms' => true,
            'edit_others_alarms' => true,
            'publish_alarms' => true,
            'read_private_alarms' => true,
            'assign_location' => true,

            'edit_activity' => true,
            'delete_activity' => true,
            'edit_activities' => true,
            'delete_activities' => true,
            'edit_others_activities' => true,
            'publish_activities' => true,
            'read_private_activities' => true,
            'assign_barracks' => true,

            'edit_traffic_info' => true,
            'delete_traffic_info' => true,
            'edit_traffic_info' => true,
            'delete_traffic_info' => true,
            'edit_others_traffic_info' => true,
            'publish_traffic_info' => true,
            'read_private_traffic_info' => true,

            'edit_vehicle' => true,
            'delete_vehicle' => true,
            'edit_vehicles' => true,
            'delete_vehicles' => true,
            'edit_others_vehicles' => true,
            'publish_vehicles' => true,
            'read_private_vehicles' => true,

            'edit_document' => true,
            'delete_document' => true,
            'edit_documents' => true,
            'delete_documents' => true,
            'edit_others_documents' => true,
            'publish_documents' => true,
            'read_private_documents' => true,
        ]);
    }

    /**
     * Is called in WP Loop
     * @param string $pageType 'list' or 'single'
     */
    public static function showVehicleFields($pageType='list')
    {
        $settings = self::getGeneralSettings();
        $fields = get_fields();
        $field_names = [
            'date' => 'Date de début',
            'end_date' => 'Date de fin',
            'ressources' => 'Ressources',
            'description' => 'Description',
            'responsable' => 'Responsable',
        ];
        $logged_in = get_current_user_id();
        foreach ($field_names as $key => $label):
            if ($pageType=='list'):
                if ( !(empty($settings['vehicle_list_show_'.$key])
                        ||  ($settings['vehicle_list_show_'.$key] == 'all')
                        || (($settings['vehicle_list_show_'.$key] == 'logged') && $logged_in)
                      ) ):
                    continue;
                endif;
            else:
                if ( !(empty($settings['vehicle_show_'.$key])
                        ||  ($settings['vehicle_show_'.$key] == 'all')
                        || (($settings['vehicle_show_'.$key] == 'logged') && $logged_in)
                      ) ):
                    continue;
                endif;
            endif;

            if (in_array($key, ['date', 'end_date']) && $fields[$key]):
                $value = self::getFRdayOfWeek(date('w', $fields[$key])) . date(' j ', $fields[$key])
                            . self::getFRmonth(date('n', $fields[$key]), false) . date(' Y, H:i', $fields[$key]);
            elseif ($key == 'ressources'):
                $value = implode(', ', $fields[$key]);
            else:
                $value = $fields[$key];
            endif;

            if (!$value):
                continue;
            endif;

            if ($pageType == 'list'): ?>
                <span class="desc"><?= $value ?></span>
            <?php else: ?>
                <div class="row">
                  <strong><?= $label ?>:</strong>
                  <span><?= $value ?></span>
                </div>
            <?php endif;
        endforeach;
    }

    public static function getYearAlarmsData($year = null)
    {
        if (empty($year) || !is_numeric($year)):
            $year = date('Y');
        endif;
        if (!isset(self::$_alarmsData[$year])):
            global $wpdb;
            $sql = 
"SELECT p.ID, p.post_title,
    pm1.meta_value AS 'date',
    pm2.meta_value AS 'description',
    pm3.meta_value AS 'number',
    pm4.meta_value AS 'images_count',
    GROUP_CONCAT(t1.name) AS 'location',
    GROUP_CONCAT(t2.name) AS 'type'
FROM {$wpdb->prefix}posts p
LEFT JOIN {$wpdb->prefix}postmeta pm1 ON pm1.post_id=p.ID AND pm1.meta_key='date'
LEFT JOIN {$wpdb->prefix}postmeta pm2 ON pm2.post_id=p.ID AND pm2.meta_key='description'
LEFT JOIN {$wpdb->prefix}postmeta pm3 ON pm3.post_id=p.ID AND pm3.meta_key='alarm_number'
LEFT JOIN {$wpdb->prefix}postmeta pm4 ON pm4.post_id=p.ID AND pm4.meta_key='images'
LEFT JOIN {$wpdb->prefix}term_relationships tr ON tr.object_id=p.ID
LEFT JOIN {$wpdb->prefix}term_taxonomy tt1 ON tr.term_taxonomy_id=tt1.term_taxonomy_id AND tt1.taxonomy='location'
LEFT JOIN {$wpdb->prefix}terms t1 ON t1.term_id = tt1.term_id
LEFT JOIN {$wpdb->prefix}term_taxonomy tt2 ON tr.term_taxonomy_id=tt2.term_taxonomy_id AND tt2.taxonomy='alarm_type'
LEFT JOIN {$wpdb->prefix}terms t2 ON t2.term_id = tt2.term_id
WHERE p.post_type='alarm'
AND p.post_status='publish'
AND pm1.meta_value LIKE '$year-%'
GROUP BY p.ID
ORDER BY pm1.meta_value DESC";
            self::$_alarmsData[$year] = $wpdb->get_results($sql, ARRAY_A);
        endif;
        return self::$_alarmsData[$year];
    }

    public static function yearAlarmsSummary($year = null)
    {
        $current_year = date('Y');
        if (empty($year) || !is_numeric($year)):
            $year = $current_year;
        endif;
        $alarms_data = self::getYearAlarmsData($year);
        $year_days_passed = ($year == $current_year) ? date('z')+1 : (365+date('L', $year.'-01-01'));
        $alarms_count = count($alarms_data);
        $day_alarm_rate = $alarms_count ? round($year_days_passed / $alarms_count, 1) : null;
        $last_alarm_date = $alarms_data ? $alarms_data[0]['date'] : null;
        if ($last_alarm_date):
            $days_since_last_alarm = $year_days_passed - date('z', strtotime($last_alarm_date)) - 1;
        else:
            $days_since_last_alarm = $year_days_passed;
        endif;
        $count_by_type = [];
        foreach ($alarms_data as $row):
            if (!isset($count_by_type[$row['type']])):
                $count_by_type[$row['type']] = 1;
            else:
                $count_by_type[$row['type']]++;
            endif;
        endforeach;
        $terms = get_terms([
            'taxonomy' => 'alarm_type',
            'hide_empty' => false,
            'orderby' => 'menu_order',
            'fields' => 'id=>name',
        ]);
        ?>
        <div class="atten-info">
          <?php if ($year == $current_year): ?>
          <p>Nombre de jours: <?= $days_since_last_alarm; ?></p>
          <?php endif; ?>
          <p>Moyenne annuelle: <?= $alarms_count ?></p>
          <?php if ($day_alarm_rate): ?>
          <p>Une alarme tous les <?= $day_alarm_rate ?> jours</p>
          <?php endif; ?>
        </div>
        <?php foreach ($terms as $term_id => $name): ?>
            <div class="row">
              <span><?= $name; ?></span>
              <span><?= Functions::arrayItem($count_by_type, $name, 0); ?></span>
            </div>
        <?php
        endforeach;
    }

    public static function getDatedPostsYears($post_type)
    {
        global $wpdb;
        $sql = "SELECT SUBSTRING(meta_value, 1, 4) AS year
FROM {$wpdb->prefix}postmeta pm
INNER JOIN {$wpdb->prefix}posts p ON p.ID=pm.post_id AND p.post_type='$post_type'
WHERE pm.meta_key='date'
GROUP BY year
ORDER BY year DESC";
        $years = $wpdb->get_col($sql);
        return $years;
    }

    public static function getTrafficInfoData()
    {
        global $wpdb;
        $today = date('Ymd');
        $sql = 
"SELECT p.ID, p.post_title,
    pm1.meta_value AS 'start_date',
    pm2.meta_value AS 'end_date'
FROM {$wpdb->prefix}posts p
LEFT JOIN {$wpdb->prefix}postmeta pm1 ON pm1.post_id=p.ID AND pm1.meta_key='start_date'
LEFT JOIN {$wpdb->prefix}postmeta pm2 ON pm2.post_id=p.ID AND pm2.meta_key='end_date'
WHERE p.post_type='traffic_info'
AND p.post_status='publish'
AND pm2.meta_value >= '$today'
ORDER BY pm1.meta_value ASC, pm2.meta_value ASC";
        $result = $wpdb->get_results($sql, ARRAY_A);
        return $result;
    }

    public static function downloadAlarmArchive($year)
    {
        require_once  __DIR__ . '/lib/vendor/autoload.php';
        ob_start();
        include __DIR__ . '/alarms_pdf.php';
        $html = ob_get_clean();
        $mpdf = new \Mpdf\Mpdf(['tempDir' => get_temp_dir()]);
        $mpdf->WriteHTML($html);
        $mpdf->Output('alarmes-'.$year.'.pdf', \Mpdf\Output\Destination::DOWNLOAD);
    }

    public static function downloadActivityArchive($year)
    {
        require_once  __DIR__ . '/lib/vendor/autoload.php';
        ob_start();
        include __DIR__ . '/activities_pdf.php';
        $html = ob_get_clean();
        $mpdf = new \Mpdf\Mpdf(['tempDir' => get_temp_dir()]);
        $mpdf->WriteHTML($html);
        $mpdf->Output('activites-'.$year.'.pdf', \Mpdf\Output\Destination::DOWNLOAD);
    }

    public static function getNextAlarmNumber()
    {
        global $wpdb;
        $year = date('Y');
        $sql = "SELECT MAX(pm1.meta_value)
FROM {$wpdb->prefix}postmeta pm1
INNER JOIN {$wpdb->prefix}postmeta pm2 ON pm2.post_id=pm1.post_id AND pm2.meta_key='date'
WHERE pm1.meta_key='alarm_number'
AND pm2.meta_value LIKE '$year-%'";
        $value = $wpdb->get_var($sql);
        $new_value = str_pad($value + 1, 3, '0', STR_PAD_LEFT);
        return $new_value;
    }

    public static function getYearActivitiesAndMiscData($year = null)
    {
        if (empty($year) || !is_numeric($year)):
            $year = date('Y');
        endif;
        if (!isset(self::$_activitiesAndMiscData[$year])):
            global $wpdb;
            $sql = 
"SELECT p.ID, p.post_title, p.post_type,
    pm1.meta_value AS 'date',
    pm2.meta_value AS 'description',
    pm3.meta_value AS 'images_count',
    GROUP_CONCAT(t1.name) AS 'location'
FROM {$wpdb->prefix}posts p
LEFT JOIN {$wpdb->prefix}postmeta pm1 ON pm1.post_id=p.ID AND pm1.meta_key='date'
LEFT JOIN {$wpdb->prefix}postmeta pm2 ON pm2.post_id=p.ID AND pm2.meta_key='description'
LEFT JOIN {$wpdb->prefix}postmeta pm3 ON pm3.post_id=p.ID AND pm3.meta_key='images'
LEFT JOIN {$wpdb->prefix}term_relationships tr ON tr.object_id=p.ID
LEFT JOIN {$wpdb->prefix}term_taxonomy tt1 ON tr.term_taxonomy_id=tt1.term_taxonomy_id AND tt1.taxonomy='location'
LEFT JOIN {$wpdb->prefix}terms t1 ON t1.term_id = tt1.term_id
WHERE p.post_type IN ('activity', 'miscellaneous')
AND p.post_status='publish'
AND pm1.meta_value LIKE '$year-%'
GROUP BY p.ID
ORDER BY pm1.meta_value DESC";
            self::$_activitiesAndMiscData[$year] = $wpdb->get_results($sql, ARRAY_A);
        endif;
        return self::$_activitiesAndMiscData[$year];
    }

    public static function getDocumentsData()
    {
        global $wpdb;
        $sql = "SELECT p.post_title AS 'title', pf.guid AS 'link', IFNULL(t.name, 'Divers') AS 'category'
FROM {$wpdb->prefix}posts p
INNER JOIN {$wpdb->prefix}postmeta pm ON pm.post_id=p.ID AND pm.meta_key='file'
INNER JOIN {$wpdb->prefix}posts pf ON pf.ID=pm.meta_value
LEFT JOIN {$wpdb->prefix}term_relationships tr ON tr.object_id=p.ID
LEFT JOIN {$wpdb->prefix}term_taxonomy tt ON tr.term_taxonomy_id=tt.term_taxonomy_id AND tt.taxonomy='document_category'
LEFT JOIN {$wpdb->prefix}terms t ON t.term_id=tt.term_id
WHERE p.post_type='document'
AND p.post_status='publish'
ORDER BY t.term_order IS NULL DESC, t.term_order ASC";
        $result = $wpdb->get_results($sql, ARRAY_A);
        return $result;
    }

    /**
     * Is called in WP Loop
     * @param array $settings ACF fields of the 'Organization' page
     * @param string $pageType 'list' or 'single'
     */
    public static function showMemberFields($settings, $pageType='list')
    {
        $fields = get_fields();
        $field_names = [
            'first_name' => 'First name',
            'last_name' => 'Last name',
            'rank' => 'Rank',
            'email' => 'Email',
            'phone' => 'Phone',
            'function' => 'Function',
        ];
        $logged_in = get_current_user_id();
        foreach ($field_names as $key => $label):
            if ($pageType=='list'):
                if ( !(empty($settings['member_show_'.$key])
                        ||  ($settings['member_show_'.$key] == 'all')
                        || (($settings['member_show_'.$key] == 'logged') && $logged_in)
                      ) ):
                    continue;
                endif;
            endif;

            $value = $fields[$key];

            if (!$value):
                continue;
            endif;
            if ($key == 'email'):
                echo "<a href=\"mailto:$value\" class=\"desc member-email\">$value</a>";
            elseif ($key == 'phone'):
                $value_no_spaces = preg_replace('/[^+0-9]/', '', $value);
                echo "<a href=\"tel:$value_no_spaces\" class=\"desc member-phone\">$value</a>";
            else:
                echo "<span class=\"desc\">$value</span>";
            endif;
        endforeach;
    }

    /**
     * Is called in WP Loop
     */
    public static function showAlarmFields($fields)
    {
        $settings = self::getGeneralSettings();
        $field_names = [
            'date' => 'Date début alarmes',
            'end_date' => 'Date fin alarme',
            'alarm_number' => 'Numéro',
            'type' => 'Type',
            'description' => 'Description',
            'commune' => 'Commune',
            'adresse' => 'Adresse',
            'train' => 'Train',
            'vehicle_depart_date' => 'Date départ véhicule',
            'vehicle_arrive_date' => 'Date arrivée véhicule',
            'effectif' => 'Effectif',
            'vehicules' => 'Véhicules',
        ];
        $logged_in = get_current_user_id();
        foreach ($field_names as $key => $label):
            if ( !(empty($settings['alarm_show_'.$key])
                    ||  ($settings['alarm_show_'.$key] == 'all')
                    || (($settings['alarm_show_'.$key] == 'logged') && $logged_in)
                  ) ):
                continue;
            endif;

            if ($key == 'type' && $fields[$key]):
                $value = $fields[$key]->name;
            elseif (in_array($key, ['date', 'end_date', 'vehicle_depart_date', 'vehicle_arrive_date']) && $fields[$key]):
                $value = self::getFRdayOfWeek(date('w', $fields[$key])) . date(' j ', $fields[$key])
                            . self::getFRmonth(date('n', $fields[$key]), false) . date(' Y, H:i', $fields[$key]);
            else:
                $value = $fields[$key];
            endif;

            if (!$value):
                continue;
            endif;
            ?>
            <div class="row">
              <strong><?= $label; ?>:</strong>    
              <span><?= $value; ?></span>
            </div>
            <?php
        endforeach;
    }

    /**
     * Is called in WP Loop
     */
    public static function showActivityFields($fields)
    {
        $settings = self::getGeneralSettings();
        $field_names = [
            'date' => 'Date début',
            'end_date' => 'Date fin',
            'description' => 'Désignation',
            'participants' => 'Participants',
            'emplacement' => 'Emplacement',
            'comment' => 'Commentaires',
            'exercise_director' => 'Directeur d’exercice',
            'tenue' => 'Tenue',
        ];
        $logged_in = get_current_user_id();
        foreach ($field_names as $key => $label):
            if ( !(empty($settings['activity_show_'.$key])
                    ||  ($settings['activity_show_'.$key] == 'all')
                    || (($settings['activity_show_'.$key] == 'logged') && $logged_in)
                  ) ):
                continue;
            endif;

            if (in_array($key, ['date', 'end_date']) && $fields[$key]):
                $value = self::getFRdayOfWeek(date('w', $fields[$key])) . date(' j ', $fields[$key])
                            . self::getFRmonth(date('n', $fields[$key]), false) . date(' Y, H:i', $fields[$key]);
            elseif ($key == 'tenue'):
                $value = implode(', ', $fields[$key]);
            else:
                $value = $fields[$key];
            endif;

            if (!$value):
                continue;
            endif;
            ?>
            <div class="row">
              <strong><?= $label; ?>:</strong>    
              <span><?= $value; ?></span>
            </div>
            <?php
        endforeach;
    }

    /**
     * Use together with date('w', $some_timestamp)
     * @param int $dow 0=Sunday ... 6=Saturday
     * @return string
     */
    public static function getFRdayOfWeek($dow, $ucfirst=true)
    {
        $days = [
            'dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'
        ];
        return $ucfirst ? ucfirst($days[$dow]) : $days[$dow];
    }

    /**
     * Use together with date('n', $some_timestamp)
     * @param int $month
     * @return string
     */
    public static function getFRmonth($month, $ucfirst=true)
    {
        $months = [
            1 => 'janvier',    2 => 'février',  3 => 'mars',      4 => 'avril',
            5 => 'mai',        6 => 'juin',     7 => 'juillet',   8 => 'août',
            9 => 'septembre', 10 => 'octobre', 11 => 'novembre', 12 => 'décembre'
        ];
        return $ucfirst ? ucfirst($months[$month]) : $months[$month];
    }

    public static function importActivitiesCSV()
    {
        if (!empty($_FILES['activities']) && $_FILES['activities']['error'] == '0'):
            return self::runActivitiesImport($_FILES['activities']['tmp_name']);
        endif;
        ?>
        <h1>Import activities</h1>
        <form method="post" enctype="multipart/form-data">
          <p>
            <input type="file" name="activities" />
          </p>
          <div>
            <button class="button button-primary" type="submit">Import</button>
          </div>
        </form>
        <?php
    }

    private static function runActivitiesImport($filename)
    {
        global $wpdb;
        $sql = "SELECT p.ID, p.post_title, pm1.meta_value 'date', pm2.meta_value 'end_date'
FROM {$wpdb->posts} p
LEFT JOIN {$wpdb->postmeta} pm1 ON pm1.post_id = p.ID AND pm1.meta_key = 'date'
LEFT JOIN {$wpdb->postmeta} pm2 ON pm2.post_id = p.ID AND pm2.meta_key = 'end_date'
WHERE p.post_type = 'activity'
";
        $activities = $wpdb->get_results($sql, ARRAY_A);
        
        $h = fopen($filename, 'r');
        fgetcsv($h, 500, ',', '"');
        $counts = [
            'new' => 0,
            'updated' => 0,
        ];
        $html = '<table border="1" style="border-collapse: collapse;">
<thead>
<tr>
<th>Status</th>
<th>ID</th>
<th>Sujet</th>
<th>Debut</th>
<th>Fin</th>
<th>Designation</th>
<th>Emplacement</th>
<th>Participants</th>
<th>Commentaires</th>
<th>DirecteurExercice</th>
</thead>
<tr>
<tbody>';
        while ($row = fgetcsv($h, 500, ',', '"')):
            $date = wp_date('Y-m-d H:i:s', $row[0]);
            $end_date = wp_date('Y-m-d H:i:s', $row[1]);
            $title = $row[2];
            $html .= '<tr>';
            $id = null;
            foreach ($activities as $post):
                if ($title == $post['post_title'] && $date == $post['date'] && $end_date == $post['end_date']):
                    $id = $post['ID'];
                    $counts['updated']++;
                    $html .= '<td>updated</td>';
                    break;
                endif;
            endforeach;
            if (!isset($id)):
                $id = wp_insert_post([
                    'post_title' => $title,
                    'post_status' => 'publish',
                    'post_type' => 'activity',
                ]);
                $counts['new']++;
                $html .= '<td>new</td>';
            endif;
            if (is_numeric($id)):
                update_post_meta($id, 'date', $date);
                update_post_meta($id, 'end_date', $end_date);
                update_post_meta($id, 'description', $row[3]);
                update_post_meta($id, 'emplacement', $row[4]);
                update_post_meta($id, 'participants', $row[5]);
                update_post_meta($id, 'comment', $row[6]);
                update_post_meta($id, 'exercise_director', $row[7]);
                $html .= "
<td>$id</td>
<td>$title</td>
<td>$date</td>
<td>$end_date</td>
<td>{$row[3]}</td>
<td>{$row[4]}</td>
<td>{$row[5]}</td>
<td>{$row[6]}</td>
<td>{$row[7]}</td>
";
            endif;
            $html .= '</tr>';
        endwhile;
        $html .= '</tbody></table>';
        fclose($h);
        echo "<p>{$counts['new']} posts added</p>"
            . "<p>{$counts['updated']} posts updated</p>"
            . "<style>td, th {padding: 2px 5px;}</style>"
            . $html;
    }

    public static function exportCalendarEvents()
    {
        $types = Functions::arrayItem($_GET, 'filter', []);
        $start = strtotime('today');
        $end = '';
        ChamberonneAjax::$_dateFormat = 'Ymd\THis';
        $result = ChamberonneAjax::getEventsByType(
            $types,
            $start,
            $end,
            ['id', 'title', 'start', 'end', 'type']
        );
        $prefixes = [
            'alarm' => 'Alarme - ',
            'activity' => 'Activité - ',
            'miscellaneous' => 'Divers - ',
            'officer' => 'Officier de piquet - ',
            'vehicle' => 'Ressource - ',
        ];
        header('Content-type: text/calendar');
        echo "BEGIN:VCALENDAR\r\n";
        echo "VERSION:2.0\r\n";
        echo "PRODID: Wordpress SDIS Chamberonne Exporter\r\n";
        foreach ($result as $event):
            echo "BEGIN:VEVENT\r\n";
            echo "UID:" . md5($event['id'] . '-' . $event['title']) . "\r\n";
            echo "DTSTART:{$event['start']}\r\n";
            echo "SUMMARY:{$prefixes[$event['type']]}{$event['title']}\r\n";
            echo "DTEND:{$event['end']}\r\n";
            echo "END:VEVENT\r\n";
        endforeach;
        echo "END:VCALENDAR";
        exit;
    }

}
