<?php

include_once __DIR__ . '/chamberonne-ajax-class.php';

add_action('wp_ajax_getCalendarEventsAlarms', [ChamberonneAjax::class, 'getCalendarEventsAlarms']);
add_action('wp_ajax_nopriv_getCalendarEventsAlarms', [ChamberonneAjax::class, 'getCalendarEventsAlarms']);

add_action('wp_ajax_getCalendarEventsActivities', [ChamberonneAjax::class, 'getCalendarEventsActivities']);
add_action('wp_ajax_nopriv_getCalendarEventsActivities', [ChamberonneAjax::class, 'getCalendarEventsActivities']);

add_action('wp_ajax_getCalendarEventsMiscellaneous', [ChamberonneAjax::class, 'getCalendarEventsMiscellaneous']);
add_action('wp_ajax_nopriv_getCalendarEventsMiscellaneous', [ChamberonneAjax::class, 'getCalendarEventsMiscellaneous']);

add_action('wp_ajax_getCalendarEventsOfficers', [ChamberonneAjax::class, 'getCalendarEventsOfficers']);
add_action('wp_ajax_nopriv_getCalendarEventsOfficers', [ChamberonneAjax::class, 'getCalendarEventsOfficers']);

add_action('wp_ajax_getCalendarEventsVehicles', [ChamberonneAjax::class, 'getCalendarEventsVehicles']);
add_action('wp_ajax_nopriv_getCalendarEventsVehicles', [ChamberonneAjax::class, 'getCalendarEventsVehicles']);
