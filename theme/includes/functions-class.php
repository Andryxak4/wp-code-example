<?php

class Functions
{

    public static function prettyVarDump($var, $args = array())
    {
        $defautls = array(
            'before_table_text' => '',
            'min_width' => 250,
            'skip_int_indices' => false,
            'dump_values' => true,
            'depth' => 0,
            'max-depth' => 10,
            'echo' => true,
            'show_classes_names' => true,
            'cut_keys' => false,
        );
        if (!is_array($args)) {
            $args = array();
        }
        $args = array_merge($defautls, $args);
        $args['depth'] = !empty($args['depth']) ? $args['depth'] + 1 : 1;
        $echo = $args['echo'];
        $args['echo'] = true;
        $classname_str = "";
        if (is_object($var)):
            if ($args['show_classes_names']):
                $classname_str = "<u><i>object</i>(<b>" . get_class($var) . "</b>)</u>\n";
            endif;
            $var = (array) $var;
            $color = '#00CC00';
        else: $color = '#333';
        endif;
        if ($echo === false) {
            ob_start();
        }
        if (!defined("PRETTY_VAR_DUMP_STYLES")) {
            define("PRETTY_VAR_DUMP_STYLES", 1);
            echo 
"<script>
function rightCellToggleCollapsed(){
    el = event.path[1];
    var next_td = el.nextElementSibling;
    el.classList.toggle('collapsed_parent');
    next_td.classList.toggle('collapsed');
}
</script>
<style>
    .pretty_var_dump__table { min-width:{$args['min_width']}px; width: auto; padding: 0; margin: 0; }
    .pretty_var_dump__table tr { border-bottom: 1px #AAA dotted; }
    .pretty_var_dump__table td { padding:1px; line-height: 1; }
    .pretty_var_dump__table td:first-child { text-align: right; position: relative; padding-left: 20px; }
    .pretty_var_dump__table td:first-child .corner_icon {
        position: absolute; top: 1px; left: 1px; text-align: center;
        height: calc(1em - 2px); width: calc(1em - 2px);
        border: 1px solid transparent; background-color: #EEE; cursor: pointer;
    }
    .corner_icon:before { content: '-';}
    .collapsed_parent .corner_icon:before { content: '+'; }
    .pretty_var_dump__table td:first-child .corner_icon:hover { border:1px solid #B8B8B8; }
    .pretty_var_dump__table td:nth-child(2) { border-left: 2px #4444FF dotted;}
    .pretty_var_dump__table td:first-child:hover { background-color: #DDD; }
    .pretty_var_dump__table pre { white-space: pre-line; padding:0; margin: 0; border: 0; line-height: 1; overflow: initial;}
    .pretty_var_dump__table .collapsed { display: none; }
    .pretty_var_dump__table .collapsed_parent { background-color: #CCC; }
</style>\n";
        }
        if ($args['depth']==1) { echo $args['before_table_text']; }
        $spaces = 6 * ($args['depth'] - 1);
        if (is_array($var)):
            echo str_pad("", $spaces, " ") . "$classname_str<table class=\"pretty_var_dump__table\""
                . " style='border:2px solid $color; min-width:{$args['min_width']}px;'>\n"
                . str_pad("", $spaces + 2, " ") . "<tbody>\n";
            if (empty($var)):
                echo "<tr><td style='text-align: center; font-size: 10px; '><i>empty array</i></td></tr>";
            endif;
            foreach ($var as $k => $v):
                if (!is_int($k) || !$args['skip_int_indices']):
                    if ($args['cut_keys']):
                        $k = (strlen($k) > $args['cut_keys'] + 2) ? ("<span title='$k'>" . substr($k, 0, $args['cut_keys']) . "..</span>") : $k;
                    endif;
                    $collapse_btn = !is_null($v) && !is_scalar($v) ? "<div class='corner_icon' onclick='rightCellToggleCollapsed()'></div>\n" : "";
                    $arr_count = is_array($v) && count($v)>1 ? '&nbsp;{'.count($v).'}' : '';
                    echo str_pad("", $spaces + 2, " ") . "<tr class='depth-{$args['depth']}'>\n"
                            . str_pad("", $spaces + 4, " ") . "<td>\n"
                            . $collapse_btn
                            . str_pad("", $spaces + 6, " ") . "[" . $k . "]" . $arr_count
                            . "<span onclick='rightCellToggleCollapsed()'>=&gt;</span>\n"
                            . str_pad("", $spaces + 4, " ") . "</td>\n"
                            . str_pad("", $spaces + 4, " ") . "<td>\n";
                    if ($args['depth'] < $args['max-depth']):
                        self::prettyVarDump($v, $args);
                    else:
                        echo "<pre><b>Max depth reached!</b></pre>";
                    endif;
                    echo str_pad("", $spaces + 4, " ") . "</td>\n"
                            . str_pad("", $spaces + 2, " ") . "</tr><!--depth-{$args['depth']}-->\n";
                endif;
            endforeach;
            echo str_pad("", $spaces + 2, " ") . "</tbody>\n"
                    . str_pad("", $spaces, " ") . "</table>\n";
        elseif(is_bool($var) || is_null($var)):
            echo str_pad("", $spaces, " ") . "<pre>";
            var_dump($var);
            echo str_pad("", $spaces, " ") . "</pre>\n";
        else:
            if ($args['dump_values']):
                echo str_pad("", $spaces, " ") . "<pre>";
                var_dump(htmlentities($var));
                echo str_pad("", $spaces, " ") . "</pre>\n";
            else:
                echo str_pad("", $spaces, " ") . htmlentities($var) . "\n";
            endif;
        endif;
        if ($echo === false) {
            $result = ob_get_contents();
            ob_end_clean();
            return $result;
        }
    }

    public static function arrayItem($array, $item, $default=null)
    {
        if (is_array($item)) {
            $element = $array;
            foreach ($item as $key) {
                if (is_object($element) && isset($element->$key)) {
                    $element = $element->$key;
                } elseif (isset($element[$key])) {
                    $element = $element[$key];
                } else {
                    return $default;
                }
            }
            return $element;
        }
        if (is_object($array)) return isset($array->$item) ? $array->$item : $default;
        return isset($array[$item]) ? $array[$item] : $default;
    }

    public static function addLogEntry($text, $filename)
    {
        $ds = DIRECTORY_SEPARATOR;
        $filename = dirname(__DIR__).$ds.'logs'.$ds.$filename.date('-Y-m-d').'.log';
        $path = dirname($filename);
        if (!is_dir($path)):
            mkdir($path, 0777, true);
        endif;
        file_put_contents($filename, date("  == Y-m-d H:i:s P ==\n").$text."\n\n", FILE_APPEND);
    }

    public static function generateRandomString($length=10)
    {
        $randstring = '';
        $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        for ($i = 0; $i < $length; $i++) {
            $randstring .= $characters[rand(0, strlen($characters)-1)];
        }
        return $randstring;
    }

    public static function hardRemoveAction($hook, $class, $method, $priority=10)
    {
        global $wp_filter;
        if (!self::arrayItem($wp_filter, array($hook, 'callbacks', $priority))):
            return;
        endif;
        foreach ($wp_filter[$hook]->callbacks[$priority] as $i => $callback):
            if (is_array($callback['function']) && ($callback['function'][0] instanceof $class)
                    && $callback['function'][1] == $method):
                unset($wp_filter[$hook]->callbacks[$priority][$i]);
                break;
            endif;
        endforeach;
    }

    public static function registerPostType($name, $options=[])
    {
        if (isset($options['plural'])):
            $plural = $options['plural'];
        else:
            $plural = $name . 's';
        endif;
        $Name = ucwords($name);
        $Plural = ucwords($plural);
        $labels = array(
            'name'                  => __($Plural, 'chamberonne'),
            'singular_name'         => __($Name, 'chamberonne'),
            'menu_name'             => __($Plural, 'chamberonne'),
            'name_admin_bar'        => __($Name, 'chamberonne'),
            'add_new_item'          => __("Add New $Name", 'chamberonne'),
            'new_item'              => __("New $Name", 'chamberonne'),
            'edit_item'             => __("Edit $Name", 'chamberonne'),
            'view_item'             => __("View $Name", 'chamberonne'),
            'all_items'             => __("All $Plural", 'chamberonne'),
            'search_items'          => __("Search $Plural", 'chamberonne'),
            'parent_item_colon'     => __("Parent $Plural:", 'chamberonne'),
            'not_found'             => __("No $plural found.", 'chamberonne'),
            'not_found_in_trash'    => __("No $plural found in Trash.", 'chamberonne'),
            'archives'              => __("$Name archives", 'chamberonne'),
            'insert_into_item'      => __("Insert into $name", 'chamberonne'),
            'uploaded_to_this_item' => __("Uploaded to this $name", 'chamberonne'),
            'filter_items_list'     => __("Filter $plural list", 'chamberonne'),
            'items_list_navigation' => __("$Plural list navigation", 'chamberonne'),
            'items_list'            => __("$Plural list", 'chamberonne'),
        );
        $postTypeName = str_replace(' ', '_', $name);
        $args = array_merge([
            'labels' => $labels,
            'public' => true,
            'has_archive' => true,
        ], self::arrayItem($options, 'args', []));
        register_post_type($postTypeName, $args);
    }

    public static function registerTaxonomy($name, $object_type, $options=[])
    {
        if (isset($options['plural'])):
            $plural = $options['plural'];
        else:
            $plural = $name . 's';
        endif;
        $Name = ucwords($name);
        $Plural = ucwords($plural);
        $labels = [
            'name'                       => __($Plural, 'chamberonne'),
            'singular_name'              => __($Name, 'chamberonne'),
            'search_items'               => __("Search $plural", 'chamberonne'),
            'popular_items'              => __("Popular $plural", 'chamberonne'),
            'all_items'                  => __("All $plural", 'chamberonne'),
            'edit_item'                  => __("Edit $name", 'chamberonne'),
            'view_item'                  => __("View $name", 'chamberonne'),
            'update_item'                => __("Update $name", 'chamberonne'),
            'add_new_item'               => __("Add new $name", 'chamberonne'),
            'new_item_name'              => __("New $name name", 'chamberonne'),
            'separate_items_with_commas' => __("Separate $plural with commas", 'chamberonne'),
            'add_or_remove_items'        => __("Add or remove $plural", 'chamberonne'),
            'choose_from_most_used'      => __("Choose from the most used $plural", 'chamberonne'),
            'not_found'                  => __("No $plural found", 'chamberonne'),
            'no_terms'                   => __("No $plural", 'chamberonne'),
            'back_to_items'              => __("&larr; Back to $plural", 'chamberonne'),
        ];
        $taxonomyName = str_replace(' ', '_', $name);
        $args = array_merge([
            'labels' => $labels,
        ], self::arrayItem($options, 'args', []));
        register_taxonomy($taxonomyName, $object_type, $args);
    }

    public static function dumpQueriesCount($prefix_text='')
    {
        echo "<pre>" . $prefix_text . get_num_queries()." queries in " . timer_stop(0) . " seconds.</pre>";
    }

}
