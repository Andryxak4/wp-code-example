<?php
get_header();
$year = Functions::arrayItem($_GET, 'y', date('Y'));
$general_settings = Chamberonne::getGeneralSettings();
$alarms_data = Chamberonne::getYearAlarmsData($year);
$alarms_years = Chamberonne::getDatedPostsYears('alarm');
?>
  <?php if (!empty($general_settings['alarms_banner'])): ?>
  <div class="banner mb" style="background-image: url('<?= $general_settings['alarms_banner'] ?>')"></div>
  <?php endif; ?>
  <section class="container">
    <div class="wrap">
      <div class="columns">
        <div class="content">
          <div class="title">
            <h1><?= $general_settings['alarms_title'] ?></h1>
          </div>
          <?php if ($alarms_data): ?>
          <div class="table-list">
          <?php
          $last_month = 0;
          foreach ($alarms_data as $row):
            $timestamp = strtotime($row['date']);
            $month = date('n', $timestamp);
            if ($month != $last_month):
              if ($last_month):
                echo '</div>'; // close previous month's .block-list
              endif;
              $last_month = $month; ?>
            <div class="title"><h2><?= Chamberonne::getFRmonth($month) ?></h2></div>
            <div class="block-list">
            <?php endif; ?>
              <a href="<?= get_permalink($row['ID']) ?>" class="row">
                <span class="name"><?= $row['number'] ?> <?= $row['post_title'] ?></span>
                <span class="desc"><?= $row['description'] ?></span>
                <span class="country"><?= $row['location'] ?></span>
                <div class="datetime">
                  <?= Chamberonne::getFRdayOfWeek(date('w', $timestamp)) . date(' d.m, H:i', $timestamp); ?>
                </div>
                <?php if ($row['images_count']): ?>
                <i class="icon icon-picture"></i>
                <?php endif; ?>
              </a>
          <?php endforeach; ?>
            </div>
          </div>
          <?php else: ?>
          <p>Pas d'alarmes</p>
          <?php endif; ?>
        </div>
        <aside class="aside">
          <div class="cont">
            <div class="info">
              <div class="title">
                <h4>Alarmes <?= $year ?></h4>
              </div>
              <div class="info-alarms">
                <?php Chamberonne::yearAlarmsSummary($year); ?>
              </div>
            </div>
          </div>
          <?php if ($alarms_years && (count($alarms_years)>1 || $alarms_years[0]!=$year)): ?>
          <div class="block-archives">
            <div class="title">
              <h4>Archives</h4>
            </div>
            <ul>
              <?php foreach ($alarms_years as $val): ?>
              <?php if ($val != $year): ?>
              <li><a href="<?= home_url('/download-alarms?y='.$val) ?>">Alarmes <?= $val ?></a></li>
              <?php endif; ?>
              <?php endforeach; ?>
            </ul>
          </div>
          <?php endif; ?>
        </aside>
      </div>
    </div>
  </section>
<?php
get_footer();
