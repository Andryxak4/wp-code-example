<?php
$general_settings = Chamberonne::getGeneralSettings();
$menu_html = wp_nav_menu([
    'echo' => false,
    'menu' => 'primary',
    'container' => 'nav',
    'container_class' => 'nav',
]);
$user_id = get_current_user_id();
if ($user_id):
    $profile_page_id = Chamberonne::getPageIdByTemplate('page-templates/profile.php');
    $login_btn_url = get_permalink($profile_page_id);
else:
    $login_btn_url = wp_login_url();
endif;
$login_btn_class = 'connect' . (is_page_template('page-templates/profile.php') ? ' current' : '');
$menu_html .= '<a href="'.$login_btn_url.'" class="'.$login_btn_class.'">
  <i class="icon icon-profile"></i>
  <span>'.$general_settings['login_button_title'].'</span>
</a>';
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <?php wp_head(); ?>
    <meta charset="utf-8">
    <meta name="robots" content="noindex,nofollow">
    <meta name="theme-color" content="#fff"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
<!--    <link rel="shortcut icon" href="img/favicons/16.png"/>
    <link rel="apple-touch-icon" href="img/favicons/60.png"/>
    <link rel="apple-touch-icon" sizes="76x76" href="img/favicons/76.png"/>
    <link rel="apple-touch-icon" sizes="120x120" href="img/favicons/120.png"/>
    <link rel="apple-touch-icon" sizes="152x152" href="img/favicons/152.png"/> -->
    <title><?php wp_title() ?></title>
    <meta name="description" content="">
    <meta property="og:title" content="<?php wp_title() ?>"/>
    <meta property="og:description" content=""/>
<!--    <meta property="og:image" content="img/favicons/opengraph.png"/> -->
<!--[if IE]>
    <script>
      document.createElement('header');
      document.createElement('nav');
      document.createElement('main');
      document.createElement('section');
      document.createElement('article');
      document.createElement('aside');
      document.createElement('footer');
    </script>
<![endif]-->
  </head>
  <body>
    <header class="header">
      <div class="info">
        <div class="wrap">
          <a href="mailto:<?= $general_settings['contact_email']; ?>"><?= $general_settings['contact_email']; ?></a>
        </div>
      </div>
      <div class="wrap">
        <div class="flex">
          <a href="<?= home_url() ?>" class="logo">
            <img src="<?= $general_settings['header_logo']; ?>" alt="<?php bloginfo('name'); ?>">
          </a>
          <div class="action">
            <?= $menu_html ?>
            <div class="hamburger">
              <span class="line"></span>
              <span class="line"></span>
              <span class="line"></span>
            </div>
          </div>
        </div>
      </div>
    </header>
    <div class="shadow"></div>
    <div class="m-panel">
      <div class="content">
        <span class="close icon-signs"></span>
        <?= $menu_html ?>
      </div>
    </div>
    <div class="wrapper">
      <main>