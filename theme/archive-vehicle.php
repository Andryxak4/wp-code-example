<?php
get_header();
$general_settings = Chamberonne::getGeneralSettings();
$posts_by_barracks = [];
$all_posts = [
    'name' => 'Tous',
    'posts' => [],
];
while (have_posts()):
    the_post();
    $thumb_id = get_post_thumbnail_id();
    $thumb_url = wp_get_attachment_image_url($thumb_id, 'medium');
    ob_start();
    ?>
    <a href="<?php the_permalink() ?>" class="item-posts">
      <div class="img"<?= $thumb_url ? ' style="background-image: url(\''.$thumb_url.'\'"' : '' ?>></div>
      <span class="title-post"><?php the_title(); ?></span>
      <?php Chamberonne::showVehicleFields('list'); ?>
    </a>
    <?php
    $html = ob_get_clean();

    $terms = wp_get_object_terms(get_the_ID(), 'barracks', ['fields' => 'id=>name']);
    foreach ($terms as $term_id => $term_name):
        $post_key = get_the_title() . '-' . get_the_ID();
        $all_posts['posts'][$post_key] = $html;
        $posts_by_barracks[$term_id]['name'] = $term_name;
        $posts_by_barracks[$term_id]['posts'][$post_key] = $html;
    endforeach;
endwhile;
// == usort doesn't keep array keys, so we don't have term IDs after this ==
// it's ok, we don't need them. they are needed only for grouping during WP Loop
usort($posts_by_barracks, function($a, $b) {
    return strcmp($a['name'], $b['name']);
});
array_unshift($posts_by_barracks, $all_posts);

$barracks_count = count($posts_by_barracks);
for ($i=0; $i<$barracks_count; $i++):
    ksort($posts_by_barracks[$i]['posts'], SORT_NATURAL | SORT_FLAG_CASE);
endfor;
?>

<?php if (!empty($general_settings['vehicles_banner'])): ?>
<div class="banner" style="background-image: url('<?= $general_settings['vehicles_banner'] ?>')"></div>
<?php endif; ?>
<?php if ($posts_by_barracks[0]['posts']): ?>
<div class="block-tabs">
  <section class="container vehicles-section">
    <div class="wrap">
      <section class="section-text single-text">
        <div class="title">
          <h1><?= $general_settings['vehicles_page_title'] ?></h1>
        </div>
        <div class="editor">
          <?= $general_settings['vehicles_page_content'] ?>
        </div>
        <div class="action-btn flex links-tab cart-tabs">
          <?php for ($i=0; $i<$barracks_count; $i++): ?>
          <a href="#tab-<?= $i ?>" class="link btn"><?= $posts_by_barracks[$i]['name'] ?></a>
          <?php endfor; ?>
        </div>
      </section>
    </div>
  </section>
  <section class="vehicles-posts">
    <div class="wrap">
      <?php foreach ($posts_by_barracks as $i => $term_data): ?>
      <div class="block-posts flex<?= $i==0 ? ' cart-tabs__item--active' : ''; ?>"
           id="tab-<?= $i ?>" <?= $i==0 ? '' : ' style="display: none;"'; ?>>
        <?= implode("\n", $term_data['posts']); ?>
      </div>
      <?php endforeach; ?>
    </div>
  </section>
</div>
<?php else: ?>
  <p>Pas de véhicules</p>
<?php endif; ?>

<?php
get_footer();
