<?php
$query = new WP_Query([
    'post_type' => 'activity',
    'post_status' => 'publish',
    'posts_per_page' => 5,
    'meta_key' => 'date',
    'orderby' => 'meta_value',
    'order' => 'desc',
    'meta_query' => [
        [
            'key' => 'date',
            'value' => date('Y-m-d H:i:s'),
            'compare' => '>'
        ],
    ],
]);
$title = 'Prochaines activités';
$all_posts_text = 'Toutes les activités';
$all_posts_url = get_post_type_archive_link('activity');
?>
<div class="cont">
  <div class="info">
    <div class="title">
      <h4><?= $title ?></h4>
    </div>
    <div class="list-alarms">
      <?php while ($query->have_posts()): ?>
      <?php $query->the_post(); ?>
      <div class="row">
        <span class="date"><?= date('d.m.y', get_field('date', $query->post->ID)); ?></span>
        <a class="text" href="<?= get_permalink($query->post) ?>"><?= get_the_title($query->post); ?></a>
      </div>
      <?php endwhile; ?>
      <?php wp_reset_postdata(); ?>
    </div>
    <div class="block-btn">
      <a href="<?= $all_posts_url ?>" class="btn"><?= $all_posts_text; ?></a>
    </div>
  </div>
</div>