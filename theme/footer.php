<?php
$general_settings = Chamberonne::getGeneralSettings();
?>
  </main>
  <footer class="footer">
    <div class="wrap">
      <div class="flex">
        <a href="<?= home_url() ?>" class="logo">
          <img src="<?= $general_settings['footer_logo']; ?>" alt="<?php bloginfo('name'); ?>" class="white">
        </a>
        <div class="footer-info">
          <?= $general_settings['footer_text'] ?>
        </div>
        <div class="copyright">
          <p><?= $general_settings['footer_copyright'] ?></p>
        </div>
      </div>
    </div>
  </footer>
</div><!-- .wrapper -->

<?php
wp_footer();
if (!empty($_GET['performance_dump'])):
    Functions::dumpQueriesCount('line ' . __LINE__ . ' in file ' . __FILE__ . PHP_EOL);
endif;
?>
</body>
</html>
