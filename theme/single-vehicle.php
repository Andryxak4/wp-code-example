<?php
get_header();
$general_settings = Chamberonne::getGeneralSettings();
while (have_posts()):
  the_post();
  $fields = get_fields();
  $thumb_id = get_post_thumbnail_id();
  $thumb_url = wp_get_attachment_image_url($thumb_id, 'medium');
  $banner = $fields['banner']
                ?: $general_settings['single_vehicle_banner']
                ?: $general_settings['vehicles_banner'];
  ?>
  <?php if ($banner): ?>
  <div class="banner" style="background-image: url('<?= $banner ?>')"></div>
  <?php endif; ?>
  <section class="single-vehicle">
    <div class="wrap">
      <div class="block-info flex">
        <div class="text">
          <div class="title">
            <h1><?php the_title(); ?></h1>
          </div>
          <div class="point-desc">
            <div class="info-list info-active">
              <?php Chamberonne::showVehicleFields('single'); ?>
            </div>
            <a href="<?= get_post_type_archive_link('vehicle'); ?>" class="btn"><?= $general_settings['all_vehicles_button_title']; ?></a>
          </div>
        </div>
        <?php if ($thumb_url): ?>
        <div class="img" style="background-image: url('<?= $thumb_url ?>')"></div>
        <?php endif; ?>
      </div>
    </div>
  </section>
<?php
endwhile;
get_footer();
