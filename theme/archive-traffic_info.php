<?php
get_header();
$general_settings = Chamberonne::getGeneralSettings();
$traffic_info_data = Chamberonne::getTrafficInfoData();
?>
<?php if (!empty($general_settings['traffic_info_banner'])): ?>
<div class="banner mb" style="background-image: url('<?= $general_settings['traffic_info_banner'] ?>')"></div>
<?php endif; ?>
<section class="container">
  <div class="wrap">
    <div class="columns">
      <div class="content">
        <div class="title">
          <h1><?= $general_settings['traffic_info_title'] ?></h1>
        </div>
        <div class="traffic-info block-tabs table-list">
          <?php if ($traffic_info_data): ?>
          <div class="block-list">
            <?php foreach ($traffic_info_data as $row): ?>
            <?php $start = strtotime($row['start_date']); ?>
            <?php $end = strtotime($row['end_date']); ?>
            <a href="<?= get_permalink($row['ID']); ?>" class="row">
              <span class="name"><?= $row['post_title']; ?></span>
              <span class="dates"><?= date('j ', $start).Chamberonne::getFRmonth(date('n', $start))
                                    . date(' - j ', $end).Chamberonne::getFRmonth(date('n', $end)); ?></span>
            </a>
            <?php endforeach; ?>
          </div>
          <?php else: ?>
          <div>Pas d'info trafic</div>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
</section>
<?php
get_footer();
