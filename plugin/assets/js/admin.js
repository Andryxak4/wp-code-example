jQuery(function($) {
  function wpAjax($el, action, data, success, error) {
    data.action = action;
    var noaction = function() {};
    $el.attr('disabled', 'disabled');
    return $.ajax(shomeatefilaOptions.ajaxurl, {
      type: 'post',
      dataType: 'json',
      data: data,
      success: success || noaction,
      error: error || noaction,
      complete: function() {
        $el.removeAttr('disabled');
      }
    });
  }
  
  var $table = $('.shomeatefila-editable-table');
  $table.editableTableWidget();
  $table.on('change', 'td', function (e, value) {
    var $this = $(this);
    if ($(e.target).is('select')) {
      value = e.target.value;
    }
    var attr = $this.data('attr') || e.target.name;
    if (!attr) {
      return;
    }
    var user_id = $this.closest('tr').data('id');
    wpAjax($this, 'shomeatefilaEditUser', {
      user_id: user_id,
      attr: attr,
      value: value
    });
  });

  // Update - send to Mailchimp
  $(document).on('click', '.shomeatefila-users-table .j-update-btn', function() {
    var $this = $(this);
    var user_id = $this.closest('tr').data('id');
    wpAjax($this, 'shomeatefilaUpdateUser', {
      user_id: user_id
    });
  });

  function removeUserById(a, id) {
    var shift = false;
    for (var i=0; i<a.length - 1; i++) {
      if (a[i].id == id) shift = true;
      if (shift) a[i] = a[i+1];
    }
    a.pop();
  }
  // Delete - make dropped
  $(document).on('click', '.shomeatefila-users-table .j-delete-user-btn', function() {
    if (!confirm('Delete User - Are you sure?')) return;
    var $this = $(this);
    $row = $this.closest('tr');
    var user_id = $row.data('id');
    wpAjax($this, 'shomeatefilaDeleteUser', {
      user_id: user_id
    }, function(response) {
      if (response.success) {
        $row.remove();
        removeUserById(shomeatefilaUsers, user_id);
      }
    });
  });

  // Auto Match - run the same algorhitm as on registration
  $(document).on('click', '.shomeatefila-users-table .j-auto-match-btn', function() {
    if (!confirm('Auto Match - Are you sure?')) return;
    var $this = $(this);
    var user_id = $this.closest('tr').data('id');
    wpAjax($this, 'shomeatefilaAutoMatchUser', {
      user_id: user_id
    },
    function() {
      location.reload();
    });
  });

  // Delete Match - set archived in Matched list, delete match from our database
  $(document).on('click', '.shomeatefila-users-table .j-delete-match-btn', function() {
    if (!confirm('Delete Match - Are you sure?')) return;
    var $this = $(this);
    var user_id = $this.closest('tr').data('id');
    wpAjax($this, 'shomeatefilaDeleteMatch', {
      user_id: user_id
    },
    function() {
      location.reload();
    });
  });

  // Set Match - select user from dropdown
  $(document).on('click', '.shomeatefila-users-table .j-manual-match-btn', function() {
    var $this = $(this),
      $col1 = $this.closest('.j-match-col'),
      $col2 = $col1.siblings('.j-manual-match-users');
    
    var user_id = $this.closest('tr').data('id');
    var i, html = '<option selected disabled value="">Select a user</option>';
    for (i=0; i<shomeatefilaUsers.length; i++) {
      if (shomeatefilaUsers[i].id != user_id) {
        html += '<option value="'+shomeatefilaUsers[i].id+'">'+shomeatefilaUsers[i].value+'</option>';
      }
    }
    $col2.find('.j-manual-match-dropdown').html(html);
    $col1.hide();
    $col2.show();
  });
  // Manual match - cancel
  $(document).on('click', '.shomeatefila-users-table .j-cancel-manual-match', function() {
    var $this = $(this),
      $col2 = $this.closest('.j-manual-match-users'),
      $col1 = $col2.siblings('.j-match-col');
    $col2.hide();
    $col1.show();
  });
  // Manual match - confirm
  $(document).on('click', '.shomeatefila-users-table .j-set-manual-match', function() {
    var $this = $(this),
      $col2 = $this.closest('.j-manual-match-users'),
      $col1 = $col2.siblings('.j-match-col'),
      $select = $col2.find('.j-manual-match-dropdown'),
      user2 = $select.val();
    if (!user2) return;
    if (!confirm('Set Match - Are you sure?')) return;
    var user1 = $this.closest('tr').data('id');
    wpAjax($this, 'shomeatefilaManualMatch', {
      user1: user1,
      user2: user2
    },
    function() {
      location.reload();
    });
  });

});
