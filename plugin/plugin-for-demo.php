<?php
/*
Plugin Name: Plugin for demo
Description: This plugin handles CF7 forms submitting, saves users to database, sends them to Mailchimp
*/

define('SHOMEATEFILA_PLUGIN_URL', WP_PLUGIN_URL . '/' . basename(__DIR__));

require_once __DIR__ . '/includes/common.php';
require_once __DIR__ . '/includes/ajax.php';
require_once __DIR__ . '/includes/table/base-table-entry-class.php';
require_once __DIR__ . '/includes/table/user-class.php';
require_once __DIR__ . '/includes/table/match-class.php';
require_once __DIR__ . '/includes/table/dropped-class.php';
require_once __DIR__ . '/includes/mailchimp-api.php';
require_once __DIR__ . '/includes/admin/shomeatefila-admin-class.php';
require_once __DIR__ . '/includes/admin/menu-pages.php';

require_once __DIR__ . '/includes/shomeatefila-class.php';

register_activation_hook(__FILE__, [Shomeatefila::class, 'onActivate']);

$plugin = plugin_basename(__FILE__);
add_filter( "plugin_action_links_$plugin", function($links) {
    $link = "<a href='".admin_url('admin.php?page=shomeatefila-settings')."'>Settings</a>";
    $links[] = $link;
    return $links;
});

add_action('wpcf7_before_send_mail', [Shomeatefila::class, 'CF7OnSubmit']);
