<?php

add_action('wp_ajax_shomeatefilaEditUser', [ShomeatefilaAdmin::class, 'ajaxEditUser']);
add_action('wp_ajax_shomeatefilaUpdateUser', [ShomeatefilaAdmin::class, 'ajaxUpdateUser']);
add_action('wp_ajax_shomeatefilaDeleteUser', [ShomeatefilaAdmin::class, 'ajaxDeleteUser']);
add_action('wp_ajax_shomeatefilaAutoMatchUser', [ShomeatefilaAdmin::class, 'ajaxAutoMatchUser']);
add_action('wp_ajax_shomeatefilaManualMatch', [ShomeatefilaAdmin::class, 'ajaxManualMatch']);
add_action('wp_ajax_shomeatefilaDeleteMatch', [ShomeatefilaAdmin::class, 'ajaxDeleteMatch']);
