<?php

abstract class ShomeatefilaBaseTableEntry
{

    protected static $table;
    protected static $primaryKey = 'id';
    protected $_pk;
    protected $_attributes = [];

    abstract public static function defaultAttributes();

    private function __construct($attributes)
    {
        $attributes = array_merge(static::defaultAttributes(), $attributes);
        $pk = $attributes[static::$primaryKey];
        $this->_pk = $pk;
        $this->_attributes = $attributes;
    }

    /**
     * 
     * @param array $attributes
     * @return \static
     */
    private static function build($attributes)
    {
        return new static($attributes);
    }

    public static function tableName()
    {
        global $wpdb;
        return $wpdb->prefix . static::$table;
    }

    /**
     * 
     * @global wpdb $wpdb
     * @param string $sql
     * @return \static
     */
    public static function findBySql($sql)
    {
        global $wpdb;
        $row = $wpdb->get_row($sql, ARRAY_A);
        if ($row) {
            $model = static::build($row);
            return $model;
        }
        return null;
    }

    /**
     * 
     * @param int|string $pk
     * @return \static
     */
    public static function findByPk($pk)
    {
        $attributes = [static::$primaryKey => $pk];
        return static::findByAttributes($attributes);
    }

    /**
     * 
     * @global wpdb $wpdb
     * @param array $attributes
     * @return \static
     */
    public static function findByAttributes($attributes)
    {
        global $wpdb;
        $sql = "SELECT * FROM `" . static::tableName() . "`"
                . " WHERE 1=1";
        $params = [];
        foreach ($attributes as $key => $value) {
            if (is_null($value)) {
                $sql .= " AND `$key` IS NULL";
            } else {
                $sql .= " AND `$key` = %s";
                $params[] = $value;
            }
        }
        $sql_prepared = $wpdb->prepare($sql, $params);
        return static::findBySql($sql_prepared);
    }

    /**
     * 
     * @global wpdb $wpdb
     * @param string $sql
     * @return \static[]
     */
    public static function findAllBySql($sql)
    {
        global $wpdb;
        $rows = $wpdb->get_results($sql, ARRAY_A);
        $result = [];
        foreach ($rows as $row) {
            $result[] = static::build($row);
        }
        return $result;
    }

    public static function findAll()
    {
        $sql = "SELECT * FROM `" . static::tableName() . "`";
        return static::findAllBySql($sql);
    }

    /**
     * 
     * @global wpdb $wpdb
     * @param array $attributes
     * @return \static
     * @throws Exception
     */
    public static function create($attributes)
    {
        $attributes = array_intersect_key($attributes, static::defaultAttributes());
        global $wpdb;
        if ($wpdb->insert(static::tableName(), $attributes)) {
            $id = $wpdb->insert_id;
            $attributes[static::$primaryKey] = $id;
            $model = static::build($attributes);
            return $model;
        } else {
            throw new Exception($wpdb->last_error);
        }
    }

    /**
     * 
     * @global wpdb $wpdb
     * @param array $attributeNames
     * @return int|false
     */
    public function update($attributeNames = null)
    {
        global $wpdb;
        $attributes = $this->_attributes;
        unset($attributes[static::$primaryKey]);
        if ($attributeNames) {
            $attributes = array_intersect_key($attributes, array_flip($attributeNames));
        }
        $result = $wpdb->update(static::tableName(), $attributes, [static::$primaryKey => $this->_pk]);
        return $result;
    }

    public function delete()
    {
        global $wpdb;
        $result = $wpdb->delete(static::tableName(), [static::$primaryKey => $this->_pk]);
        return $result;
    }

    public function __get($name)
    {
        if (array_key_exists($name, $this->_attributes)) {
            return $this->_attributes[$name];
        } else {
            throw new Exception("Wrong attribute: $name");
        }
    }

    public function __set($name, $value)
    {
        if (array_key_exists($name, $this->_attributes)) {
            $this->_attributes[$name] = $value;
        } else {
            throw new Exception("Wrong attribute: $name");
        }
    }

}
