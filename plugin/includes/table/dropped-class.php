<?php

/**
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $hebrew_name
 * @property string $gender
 * @property string $email
 * @property string $prayer_type
 * @property int $created_at
 * @property int $dropped_at
 * @property string $user_track
 */
class ShomeatefilaDropped extends ShomeatefilaBaseTableEntry
{

    protected static $table = 'shomeatefila_dropped';

    public static function defaultAttributes()
    {
        return [
            'id' => null,
            'first_name' => null,
            'last_name' => null,
            'hebrew_name' => null,
            'gender' => null,
            'email' => null,
            'prayer_type' => null,
            'created_at' => null,
            'dropped_at' => null,
            'user_track' => null,
        ];
    }

}
