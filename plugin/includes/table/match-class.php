<?php

/**
 * @property int $id
 * @property int $user1
 * @property int $user2
 * @property int $created_at
 * @property int $updated_at
 */
class ShomeatefilaMatch extends ShomeatefilaBaseTableEntry
{

    protected static $table = 'shomeatefila_matches';

    public static function defaultAttributes()
    {
        return [
            'id' => null,
            'user1' => null,
            'user2' => null,
            'created_at' => null,
            'updated_at' => null,
        ];
    }

    /**
     * 
     * @global wpdb $wpdb
     * @param integer $user_id
     * @return \self
     */
    public static function findByUserId($user_id)
    {
        global $wpdb;
        $sql = "SELECT * FROM `".ShomeatefilaMatch::tableName()."`"
                . " WHERE %d IN (user1, user2)";
        $sql_prepared = $wpdb->prepare($sql, $user_id);
        return self::findBySql($sql_prepared);
    }

}
