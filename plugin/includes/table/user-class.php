<?php

/**
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $hebrew_name
 * @property string $gender
 * @property string $email
 * @property string $prayer_type
 * @property int $created_at
 * @property string $feedback_answer
 * @property string $user_track
 */
class ShomeatefilaUser extends ShomeatefilaBaseTableEntry
{

    protected static $table = 'shomeatefila_users';

    public static function defaultAttributes()
    {
        return [
            'id' => null,
            'first_name' => null,
            'last_name' => null,
            'hebrew_name' => null,
            'gender' => null,
            'email' => null,
            'prayer_type' => null,
            'created_at' => null,
            'feedback_answer' => null,
            'user_track' => null,
        ];
    }

    /**
     * 
     * @global wpdb $wpdb
     * @return string
     */
    public static function generateUniqueTrack()
    {
        global $wpdb;
        $sql = "SELECT user_track FROM `" . self::tableName() . "`";
        $user_tracks = $wpdb->get_col($sql);
        do {
            $random_str = generateRandomString();
        } while (in_array($random_str, $user_tracks));
        return $random_str;
    }

    public function getMailchimpId()
    {
        return md5(strtolower($this->email));
    }

    public function addToPool()
    {
        $this->sendToMailchimp(Shomeatefila::settings('mailchimp-list-pool'));
    }

    public function returnToPool()
    {
        $fields = [
            'HNAME' => '',
            'ISCHANGED' => true
        ];
        $this->sendToMailchimp(Shomeatefila::settings('mailchimp-list-pool'), $fields);
    }

    /**
     * 
     * @return boolean True if matched user found, false otherwise
     */
    public function autoMatch()
    {
        $matchedUser = $this->lookForMatch();
        if ($matchedUser) {
            $this->matchFound($matchedUser);
            return true;
        } else {
            $this->matchNotFound();
            return false;
        }
    }

    /**
     * 
     * @param string $list_id
     * @param array $fields
     * @return array
     */
    public function sendToMailchimp($list_id, $fields = [], $subscribe = true)
    {
        $data = [
            'email_address' => $this->email,
            'status_if_new' => 'subscribed',
            'merge_fields' => array_merge([
                'FNAME' => $this->first_name,
                'LNAME' => $this->last_name,
                'GENDER' => $this->gender,
                'HNAME' => $this->hebrew_name,
                'PTYPE' => $this->prayer_type,
                'USER_TRACK' => $this->user_track,
            ], $fields),
        ];
        if ($subscribe) {
            $data['status'] = 'subscribed';
        }
        $subscriber_hash = $this->getMailchimpId();
        try {
            Shomeatefila::mc()->addOrUpdateListMember($list_id, $subscriber_hash, $data);
        } catch (MailchimpException $e) {
            addLogEntry("User #{$this->id} {$this->email}, list {$list_id}\n".$e->getMessage()."\n".$e->getTraceAsString(),
                    'mailchimp.exception.senduser');
        }
    }

    public function makeArchived($list_id)
    {
        $subscriber_hash = $this->getMailchimpId();
        $result = false;
        try {
            $result = Shomeatefila::mc()->archiveListMember($list_id, $subscriber_hash);
        } catch (MailchimpException $e) {
            addLogEntry("User #{$this->id} {$this->email}, list {$list_id}\n".$e->getMessage()."\n".$e->getTraceAsString(),
                    'mailchimp.exception.archive');
        }
        return $result;
    }

    public function sendMailchimpMatchEmail()
    {
        $automation_id = Shomeatefila::settings('mailchimp-matched-automation');
        $email_id = Shomeatefila::settings('mailchimp-matched-automation-email');
        $result = null;
        if ($automation_id && $email_id) {
            try {
               $result = Shomeatefila::mc()->addAutomationEmailQueue($automation_id, $email_id, $this->email);
            } catch (MailchimpException $e) {
                addLogEntry("User #{$this->id} {$this->email}\n".$e->getMessage()."\n".$e->getTraceAsString(),
                        'mailchimp.exception.emailqueue');
            }
        }
        return $result;
    }

    /**
     * Find user with the same gender, the same prayer_type and that doesn't have a partner (match) yet
     * @global wpdb $wpdb
     * @return \self
     */
    public function lookForMatch()
    {
        global $wpdb;
        $sql = "SELECT u.* FROM `" . self::tableName() . "` u
LEFT JOIN `" . ShomeatefilaMatch::tableName() . "` m ON u.id IN (m.user1, m.user2)
WHERE (m.id IS NULL OR (u.feedback_answer = %s AND m.updated_at < %d))
AND u.id <> %d
AND u.email <> %s
AND u.gender = %s
AND u.prayer_type = %s
ORDER BY RAND()
LIMIT 1";
        $min_active_match_time = time() - 144*24*3600; // max pair time = 3 month + 7 days + 7 days + 30 days = 144 days
        $sql_prepared = $wpdb->prepare($sql, Shomeatefila::FEEDBACK_ANSWER_CONTINUE, $min_active_match_time, $this->id, $this->email, $this->gender, $this->prayer_type);
        return self::findBySql($sql_prepared);
    }

    public function matchNotFound()
    {
        // no action needed
        // Mailchimp Pool "Welcome" email is also "Waiting for match" email
    }

    /**
     * 
     * @param \static $matchedUser
     */
    public function matchFound($matchedUser)
    {
        if ($match=$matchedUser->getRelatedMatch()) {
            //if $matchedUser had partner before, but he hasn't responded on all emails, so pair became expired
            $match->delete();
            $matchedUser->feedback_answer = null;
            $matchedUser->update(['feedback_answer']);
        }
        ShomeatefilaMatch::create([
            'user1' => $this->id,
            'user2' => $matchedUser->id,
            'created_at' => $this->created_at,
            'updated_at' => $this->created_at,
        ]);

        $poolList = Shomeatefila::settings('mailchimp-list-pool');
        $matchedList = Shomeatefila::settings('mailchimp-list-matched');
        $matchedSegment = Shomeatefila::settings('mailchimp-matched-segment');

        $data = [
            'PHNAME' => $matchedUser->hebrew_name,
        ];
        $this->makeArchived($poolList);
        $this->sendToMailchimp($matchedList, $data);
        $this->addToSegment($matchedList, $matchedSegment);
        $this->sendMailchimpMatchEmail();
        
        $partnerData = [
            'PHNAME' => $this->hebrew_name,
        ];
        $matchedUser->makeArchived($poolList);
        $matchedUser->sendToMailchimp($matchedList, $partnerData);
        $matchedUser->addToSegment($matchedList, $matchedSegment);
        $matchedUser->sendMailchimpMatchEmail();
    }

    /**
     * 
     * @return \self[]
     */
    public static function findAllWithMatches()
    {
        $sql = "SELECT u.*, u2.id AS m_id, u2.hebrew_name AS m_hebrew_name, u2.email AS m_email"
                . " FROM `" . self::tableName() . "` u"
                . " LEFT JOIN `" . ShomeatefilaMatch::tableName() . "` m ON u.id IN (m.user1, m.user2)"
                . " LEFT JOIN `" . self::tableName() . "` u2 ON u2.id IN (m.user1, m.user2) AND u2.id<>u.id"
                . " ORDER BY u.id";
        return self::findAllBySql($sql);
    }

    /**
     * 
     * @global wpdb $wpdb
     * @return \self
     */
    public function findMatchedUser()
    {
        global $wpdb;
        $sql = "SELECT u.*"
                . " FROM `" . self::tableName() . "` u"
                . " LEFT JOIN `" . ShomeatefilaMatch::tableName() . "` m"
                    . " ON u.id IN (m.user1, m.user2)"
                    . " AND %s IN (m.user1, m.user2)"
                    . " AND u.id<>%s";
        $sql_prepared = $wpdb->prepare($sql, $this->id, $this->id);
        return self::findBySql($sql_prepared);
    }

    /**
     * 
     * @global wpdb $wpdb
     * @return ShomeatefilaMatch
     */
    public function getRelatedMatch()
    {
        global $wpdb;
        $sql = "SELECT * FROM `" . ShomeatefilaMatch::tableName() . "`"
                . " WHERE %s IN (user1, user2)";
        $sql_prepared = $wpdb->prepare($sql, $this->id);
        return ShomeatefilaMatch::findBySql($sql_prepared);
    }

    public function makeDropped()
    {
        $attributes = $this->_attributes + ['dropped_at' => time()];
        $dropped = ShomeatefilaDropped::create($attributes);
        $this->makeArchived(Shomeatefila::settings('mailchimp-list-pool'));
        $this->makeArchived(Shomeatefila::settings('mailchimp-list-matched'));
        $result = $this->sendToMailchimp(Shomeatefila::settings('mailchimp-list-dropped'));
        $this->delete();
    }

    public function addToSegment($list_id, $segment_id)
    {
        try {
            Shomeatefila::mc()->addToSegment($list_id, $segment_id, $this->email);
        } catch (MailchimpException $e) {
            addLogEntry("User #{$this->id} {$this->email}, list {$list_id}, segment {$segment_id}\n"
                    . $e->getMessage() . "\n"
                    . $e->getTraceAsString(),
                'mailchimp.exception.addtosegment');
        }
    }

}
