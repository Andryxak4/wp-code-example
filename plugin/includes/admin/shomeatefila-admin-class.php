<?php

class ShomeatefilaAdmin
{

    private static $_CF7Forms;

    public static function CF7forms()
    {
        if (!isset(self::$_CF7Forms)) {
            $forms = get_posts([
                'post_type' => WPCF7_ContactForm::post_type,
                'numberposts' => -1,
            ]);
            $result = [];
            foreach ($forms as $form_post) {
                $result[$form_post->ID] = $form_post->post_title;
            }
            self::$_CF7Forms = $result;
        }
        return self::$_CF7Forms;
    }

    public static function MCLists()
    {
        $result = get_transient('shomeatefila_mc_lists');
        if ($result === false) {
            try {
                $lists = Shomeatefila::mc()->getLists();
            } catch (MailchimpException $e) {
                $lists = [];
            }
            $result = [];
            foreach ($lists as $list) {
                $result[$list['id']] = $list['name'];
            }
            set_transient('shomeatefila_mc_lists', $result, 600);
        }
        return $result;
    }

    public static function MCAutomations()
    {
        if (!Shomeatefila::settings('mailchimp-list-matched')) {
            return [];
        }
        $result = get_transient('shomeatefila_mc_automations');
        if ($result === false) {
            try {
                $automations = Shomeatefila::mc()->getAutomations();
            } catch (MailchimpException $e) {
                $automations = [];
            }
            $result = [];
            foreach ($automations as $item) {
                if ($item['recipients']['list_id'] == Shomeatefila::settings('mailchimp-list-matched')) {
                    $result[$item['id']] = $item['settings']['title'];
                }
            }
            set_transient('shomeatefila_mc_automations', $result, 600);
        }
        return $result;
    }

    public static function MCAutomationEmails()
    {
        $automation_id = Shomeatefila::settings('mailchimp-matched-automation');
        if (!$automation_id) {
            return [];
        }
        $result = get_transient("shomeatefila_mc_automation_{$automation_id}_workflows");
        if ($result === false) {
            try {
                $emails = Shomeatefila::mc()->getAutomationEmails($automation_id);
            } catch (MailchimpException $e) {
                $emails = [];
            }
            $result = [];
            foreach ($emails as $item) {
                $result[$item['id']] = $item['settings']['title'];
            }
            set_transient("shomeatefila_mc_automation_{$automation_id}_workflows", $result, 600);
        }
        return $result;
    }

    public static function MCMatchedSegments()
    {
        $list_id = Shomeatefila::settings('mailchimp-list-matched');
        if (!$list_id) {
            return [];
        }
        $result = get_transient('shomeatefila_mc_matched_segments');
        if ($result === false) {
            try {
                $segments = Shomeatefila::mc()->getSegments($list_id);
            } catch (MailchimpException $e) {
                $segments = [];
            }
            $result = [];
            foreach ($segments as $item) {
                $result[$item['id']] = $item['name'];
            }
            set_transient('shomeatefila_mc_matched_segments', $result, 600);
        }
        return $result;
    }

    public static function ajaxEditUser()
    {
        $user_id = $_POST['user_id'];
        $attr = $_POST['attr'];
        $value = $_POST['value'];
        $user = ShomeatefilaUser::findByPk($user_id);
        $user->$attr = $value;
        $user->update([$attr]);
        exit(json_encode(['success' => true]));
    }

    public static function ajaxUpdateUser()
    {
        $user_id = $_POST['user_id'];
        $user = ShomeatefilaUser::findByPk($user_id);
        $match = $user->getRelatedMatch();
        $user->sendToMailchimp(Shomeatefila::settings('mailchimp-list-pool'), [], false);
        // send to "Matched" list only if user is already there
        if ($match) {
            $user->sendToMailchimp(Shomeatefila::settings('mailchimp-list-matched'), [], false);
        }
        exit(json_encode(['success' => true]));
    }

    public static function ajaxDeleteUser()
    {
        $user_id = $_POST['user_id'];
        $user = ShomeatefilaUser::findByPk($user_id);
        $user->makeDropped();
        exit(json_encode(['success' => true]));
    }

    public static function ajaxAutoMatchUser()
    {
        $user_id = $_POST['user_id'];
        $user = ShomeatefilaUser::findByPk($user_id);
        try {
            $user->autoMatch();
        } catch (MailchimpException $e) {
            addLogEntry("User #{$user->id} {$user->email}\n".$e->getMessage()."\n".$e->getTraceAsString(),
                        'mailchimp.exception.ajaxautomatch');
        }
        exit(json_encode(['success' => true]));
    }

    public static function ajaxManualMatch()
    {
        $user1 = $_POST['user1'];
        $user2 = $_POST['user2'];
        $user = ShomeatefilaUser::findByPk($user1);
        $matchedUser = ShomeatefilaUser::findByPk($user2);
        try {
            $user->matchFound($matchedUser);
        } catch (MailchimpException $e) {
            addLogEntry("User #{$user->id} {$user->email}\n".$e->getMessage()."\n".$e->getTraceAsString(),
                        'mailchimp.exception.ajaxmanualmatch');
        }
        exit(json_encode(['success' => true]));
    }

    public static function ajaxDeleteMatch()
    {
        $user_id = $_POST['user_id'];
        $match = ShomeatefilaMatch::findByUserId($user_id);
        $user1 = ShomeatefilaUser::findByPk($match->user1);
        $user2 = ShomeatefilaUser::findByPk($match->user2);
        $user1->makeArchived(Shomeatefila::settings('mailchimp-list-matched'));
        $user2->makeArchived(Shomeatefila::settings('mailchimp-list-matched'));
        $match->delete();
        exit(json_encode(['success' => true]));
    }

    private static function _showTableHead($cols)
    {
        echo "<thead>"
            . "<tr>";
        foreach ($cols as $i => $col) {
            $style = $i==8 ? ' style="text-align: center;"' : '';
            echo "<th$style>$col</th>";
        }
        echo "</tr>"
            . "</thead>";
    }
    private static function _tableDropdownCell($name, $options, $selected)
    {
        $html = '<select name="'.$name.'" class="">';
        foreach ($options as $option) {
            $selected_attr = $selected == $option ? ' selected' : '';
            $html .= '<option'.$selected_attr.'>'.$option.'</option>';
        }
        $html .= '</select>';
        return $html;
    }
    private static function _tableGenderCell($gender)
    {
        $options = [
            'זכר',
            'נקבה'
        ];
        return self::_tableDropdownCell('gender', $options, $gender);
    }
    private static function _tablePrayerTypeCell($prayer_type)
    {
        $options = [
            'רפואה שלמה',
            'פרנסה בשפע',
            'תיקון המידות',
            'זווג הגון',
            'פוריות (זרע של קיימא)',
            'יראת שמים',
            'שלום בית',
            'חינוך ילדים',
            'הצלחה (כללי)',
            'בריאות (כללי)',
        ];
        return self::_tableDropdownCell('prayer_type', $options, $prayer_type);
    }

    private static function _userMatchColumn($user)
    {
        $html = '<div style="text-align: center;">'
                . '<div class="j-match-col">';
        if (!$user->m_id) {
            $html .= 'No&nbsp;&nbsp;&nbsp;'
                        . ' <button class="button j-auto-match-btn" title="Run match finder algorhitm">Auto Match</button>'
                        . ' <button class="button j-manual-match-btn" title="Select user manually">Set Match</button>';
        } else {
            $html .= "#{$user->m_id} {$user->m_hebrew_name} &lt;{$user->m_email}&gt;"
                        . " <button class='button button-link-delete j-delete-match-btn'>Delete</button>";
        }
        $html .= '</div>'
                . '<div class="j-manual-match-users" style="display: none;">'
                . '<select class="j-manual-match-dropdown"></select>'
                . ' <button class="button-primary j-set-manual-match">Set</button>'
                . ' <button class="button j-cancel-manual-match">Cancel</button>'
                . '</div>'
            . '</div>';
        return $html;
    }

    public static function logsPage()
    {
        echo "<h3>Logs</h3>";
        $logs = glob(dirname(dirname(__DIR__)).DIRECTORY_SEPARATOR.'logs'.DIRECTORY_SEPARATOR.'*');
        $cnt = count($logs);
        for ($i=0; $i < $cnt; $i++) {
            $name = basename($logs[$i]);
            echo '<a href="'.SHOMEATEFILA_PLUGIN_URL.'/logs/'.$name.'" class="button" target="_blank">'.$name.'</a><br>';
        }
    }

    public static function usersPage()
    {
        if (!empty($_GET['debug']) && $_GET['debug']=='logs') {
            return self::logsPage();
		}
        if (!empty($_GET['debug']) && $_GET['debug']=='dropped') {
			$dropped = ShomeatefilaDropped::findAll();
			echo "<pre>";
			var_export($dropped);
			echo "</pre>";
			die;
		}
        $users = ShomeatefilaUser::findAllWithMatches();

        echo "<style>"
                . ".shomeatefila-users-table th {font-weight: bold;}"
                . " .shomeatefila-users-table td {line-height: 26px;}"
                . " .shomeatefila-users-table select {vertical-align: baseline;}"
                . "</style>";
        echo "<h1>Shomeatefila&nbsp;&gt;&nbsp;Users</h1>";
        echo '<table class="widefat shomeatefila-users-table shomeatefila-editable-table">';
        self::_showTableHead([
            'Id', 'First name', 'Last name', 'Hebrew name', 'Gender', 'Email', 'Prayer type', '', 'Match', 'User Track',
        ]);
        $js_users = [];
        echo '<tbody>';
        foreach ($users as $user) {
            if (!$user->m_id) {
                $js_users[] = [
                    'id' => $user->id,
                    'value' => "#{$user->id} {$user->hebrew_name} &lt;{$user->email}&gt;",
                ];
            }
            echo "<tr data-id='{$user->id}'>"
                . "<td class='noEditor'>{$user->id}</td>"
                . "<td data-attr='first_name'>{$user->first_name}</td>"
                . "<td data-attr='last_name'>{$user->last_name}</td>"
                . "<td data-attr='hebrew_name'>{$user->hebrew_name}</td>"
                . "<td class='noEditor'>" . self::_tableGenderCell($user->gender) . "</td>"
                . "<td class='noEditor'>{$user->email}</td>"
                . "<td class='noEditor'>" . self::_tablePrayerTypeCell($user->prayer_type) . "</td>"
                . "<td class='noEditor'>"
                        . '<button class="button-primary j-update-btn" title="Send fields to Mailchimp">Update</button>'
                        . ($user->m_id ? '' : '&nbsp;<button class="button-primary j-delete-user-btn" title="Move to Dropped">Delete</button>')
                . "</td>"
                . "<td class='noEditor'>".self::_userMatchColumn($user)."</td>"
                . "<td class='noEditor'>{$user->user_track}</td>"
            . '</tr>';
        }
        echo '</tbody>';
        echo '</table>';
        
        echo '<script>var shomeatefilaUsers = '.json_encode($js_users).';</script>';
    }

    public static function settingsPage()
    {
        if (!empty($_POST['shomeatefila'])) {
            self::_saveSettings($_POST['shomeatefila']);
        }
        $fields = [
            'form-registration' => 'Registration form',
            'form-feedback' => 'Feedback form',
            'mailchimp-api-key' => 'Mailchimp API key',
        ];
        if (Shomeatefila::settings('mailchimp-api-key')) {
            $fields += [
                'mailchimp-list-pool' => 'Mailchimp: Pool list id',
                'mailchimp-list-matched' => 'Mailchimp: Matched list id',
                'mailchimp-list-dropped' => 'Mailchimp: Dropped list id',
                'mailchimp-matched-automation' => 'Mailchimp: Automation for "Match found" email',
                'mailchimp-matched-automation-email' => 'Mailchimp: "Match found" email',
                'mailchimp-matched-segment' => 'Mailchimp: Matched list tag to add',
            ];
        }
        echo "<style>.shomeatefila-settings-form input, .shomeatefila-settings-form select {width: 100%;}</style>";
        echo "<h1>Shomeatefila&nbsp;&gt;&nbsp;Settings</h1>";
        echo "<div class='form-wrap'>";
        echo "<form method='post' autocomplete='off' class='shomeatefila-settings-form'>";
        foreach ($fields as $key => $label) {
            echo "<div class='form-field'>"
                . "<label for='$key'>$label</label>"
                . self::_settingsField($key)
                . "</div>";
        }
        echo "<div><button type='submit' class='button-primary'>Save</button></div>";
        echo "</div>";
        echo "</form>";
    }

    private static function _settingsField($key)
    {
        $value = Shomeatefila::settings($key);
        if (strpos($key, 'form-') === 0) {
            $forms = self::CF7forms();
            return self::_settingsDropdownField($key, $forms, $value);
        } elseif (strpos($key, 'mailchimp-list-') === 0) {
            $lists = self::MCLists();
            return self::_settingsDropdownField($key, $lists, $value);
        } elseif ($key == 'mailchimp-matched-automation') {
            $automations = self::MCAutomations();
            return self::_settingsDropdownField($key, $automations, $value);
        } elseif ($key == 'mailchimp-matched-automation-email') {
            $emails = self::MCAutomationEmails();
            return self::_settingsDropdownField($key, $emails, $value);
        } elseif ($key == 'mailchimp-matched-segment') {
            $segments = self::MCMatchedSegments();
            return self::_settingsDropdownField($key, $segments, $value);
        } else {
            return "<input type='text' id='$key' name='shomeatefila[$key]' value='$value' required/>";
        }
    }

    private static function _settingsDropdownField($key, $options, $value)
    {
        $html = "<select id='$key' name='shomeatefila[$key]'>";
        $html .= "<option value='' disabled" . ($value ? '' : ' selected') . ">Select an option</option>";
        foreach ($options as $id=>$title) {
            $html .= "<option value='$id'" . ($id == $value ? ' selected' : '') . ">$title</option>";
        }
        $html .= "</select>";
        return $html;
    }

    private static function _saveSettings($settings)
    {
        update_option('shomeatefila-settings', json_encode($settings));
    }

}
