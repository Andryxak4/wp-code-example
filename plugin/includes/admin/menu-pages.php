<?php

add_action('admin_enqueue_scripts', function() {
    wp_enqueue_script('editable-table', SHOMEATEFILA_PLUGIN_URL . '/assets/js/mindmup-editabletable.js', ['jquery'], false, true);
    wp_enqueue_script('shomeatefile-admin-script', SHOMEATEFILA_PLUGIN_URL . '/assets/js/admin.js', ['editable-table'], false, true);
    wp_localize_script('shomeatefile-admin-script', 'shomeatefilaOptions', [
        'ajaxurl' => admin_url('admin-ajax.php'),
    ]);
});

add_action('admin_menu', function() {
    add_menu_page('Shomeatefila', 'Shomeatefila', 'read', 'shomeatefila-users', [ShomeatefilaAdmin::class, 'usersPage']);
    add_submenu_page('shomeatefila-users', 'Users', 'Users', 'read', 'shomeatefila-users');
    add_submenu_page('shomeatefila-users', 'Settings', 'Settings', 'read', 'shomeatefila-settings', [ShomeatefilaAdmin::class, 'settingsPage']);
});
