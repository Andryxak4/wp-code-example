<?php

class MailchimpException extends Exception {}
class MailchimpBadResponseException extends MailchimpException {}

class MailchimpAPI
{

    private $apiUrl;
    private $apiKey;

    private $_curlinfo;

    /**
     * 
     * @param string $api_key
     * @throws MailchimpException
     */
    public function __construct($api_key)
    {
        $this->apiKey = $api_key;
        $data_center = substr($this->apiKey, strpos($this->apiKey, '-')+1);
        if ($data_center === false) {
            throw new MailchimpException('Incorrect API key');
        }
        $this->apiUrl = 'https://' . $data_center . '.api.mailchimp.com/3.0';
    }

    /**
     * 
     * @param string $endpoint
     * @param array $data
     * @param array $options
     * @param bool $debug
     * @return array
     * @throws MailchimpBadResponseException
     */
    private function curlRequest($endpoint, $data=array(), $options=array(), $debug=false)
    {
        $url = $this->apiUrl . $endpoint;

        $ch = curl_init($url);
        if ($data) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        }

        $default_options = array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_CONNECTTIMEOUT => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTPHEADER => array("Content-Type: application/json"),
            CURLOPT_USERPWD => 'user:' . $this->apiKey,
            CURLOPT_SSL_VERIFYPEER => false,
        );

        curl_setopt_array($ch, $default_options);
        curl_setopt_array($ch, $options);
        $raw_result = curl_exec($ch);
        $this->_curlinfo = curl_getinfo($ch);
        curl_close($ch);

        $decoded = json_decode($raw_result, true);
        $result = json_last_error() ? $raw_result : $decoded;

        addLogEntry(
            "Url: $url\n"
                . "Request:\n" . var_export($data, true) . "\n"
                . ($options ? "Options:\n" . var_export($options, true) . "\n" : "")
                . "Response:\n" . var_export($result, true) . "\n",
            'mailchimp.response');

        if (isset($result['status'], $result['detail']) && $result['status'] != 200) {
            throw new MailchimpBadResponseException($result['detail'], $result['status']);
        }

        return $result;
    }

    /**
     * 
     * @return array
     * @throws MailchimpBadResponseException
     */
    public function getLists()
    {
        $response = $this->curlRequest('/lists');
        if (!isset($response['lists'])) {
            throw new MailchimpBadResponseException('No lists in API response');
        }
        return $response['lists'];
    }

    public function addListMember($list_id, $data)
    {
        return $this->curlRequest("/lists/$list_id/members", $data);
    }

    public function addOrUpdateListMember($list_id, $subscriber_hash, $data)
    {
        return $this->curlRequest("/lists/$list_id/members/$subscriber_hash", $data, [CURLOPT_CUSTOMREQUEST => 'PUT']);
    }

    public function archiveListMember($list_id, $subscriber_hash)
    {
        return $this->curlRequest("/lists/$list_id/members/$subscriber_hash", [], [CURLOPT_CUSTOMREQUEST => 'DELETE']);
    }

    public function getAutomations()
    {
        $response = $this->curlRequest('/automations');
        if (!isset($response['automations'])) {
            throw new MailchimpBadResponseException('No automations in API response');
        }
        return $response['automations'];
    }

    public function getAutomationEmails($automation_id)
    {
        $response = $this->curlRequest("/automations/$automation_id/emails");
        if (!isset($response['emails'])) {
            throw new MailchimpBadResponseException('No emails in API response');
        }
        return $response['emails'];
    }

    public function addAutomationEmailQueue($automation_id, $workflow_email_id, $email_address)
    {
        $data = ['email_address' => $email_address];
        return $this->curlRequest("/automations/$automation_id/emails/$workflow_email_id/queue", $data);
    }

    public function getSegments($list_id)
    {
        $response = $this->curlRequest("/lists/$list_id/segments");
        if (!isset($response['segments'])) {
            throw new MailchimpBadResponseException('No segments in API response');
        }
        return $response['segments'];
    }

    public function addToSegment($list_id, $segment_id, $email_address)
    {
        $data = ['email_address' => $email_address];
        return $this->curlRequest("/lists/$list_id/segments/$segment_id/members", $data);
    }

}
