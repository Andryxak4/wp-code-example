<?php

class Shomeatefila
{

    const FEEDBACK_ANSWER_CONTINUE = 'כן';
    const FEEDBACK_ANSWER_STOP = 'לא';

    private static $_settings;
    private static $_mc;

    public static function settings($key = null)
    {
        if (!isset(self::$_settings)) {
            $json = get_option('shomeatefila-settings', '[]');
            self::$_settings = json_decode($json);
        }
        if ($key) {
            return arrayItem(self::$_settings, $key);
        } else {
            return self::$_settings;
        }
    }

    /**
     * 
     * @return MailchimpAPI
     */
    public static function mc()
    {
        if (!self::$_mc) {
            self::$_mc = new MailchimpAPI(self::settings('mailchimp-api-key'));
        }
        return self::$_mc;
    }

    public static function onActivate()
    {
        global $wpdb;
        $sql_users = "CREATE TABLE IF NOT EXISTS `".ShomeatefilaUser::tableName()."` (
    id INT NOT NULL AUTO_INCREMENT,
    first_name VARCHAR(255) NOT NULL,
    last_name VARCHAR(255) NOT NULL,
    hebrew_name VARCHAR(255) NOT NULL,
    gender VARCHAR(255) NOT NULL,
    email VARCHAR(255) NOT NULL,
    prayer_type VARCHAR(255) NOT NULL,
    created_at INT NOT NULL,
    feedback_answer VARCHAR(255) NULL DEFAULT NULL,
    user_track VARCHAR(255) NULL DEFAULT NULL,
    PRIMARY KEY (id),
    UNIQUE KEY `user_track` (`user_track`)
)";
        $wpdb->query($sql_users);
        $sql_matches = "CREATE TABLE IF NOT EXISTS `".ShomeatefilaMatch::tableName()."` (
    id INT NOT NULL AUTO_INCREMENT,
    user1 INT NOT NULL,
    user2 INT NOT NULL,
    created_at INT NOT NULL,
    updated_at INT NOT NULL,
    PRIMARY KEY (id)
)";
        $wpdb->query($sql_matches);
        $sql_dropped = "CREATE TABLE IF NOT EXISTS `".ShomeatefilaDropped::tableName()."` (
    id INT NOT NULL AUTO_INCREMENT,
    first_name VARCHAR(255) NOT NULL,
    last_name VARCHAR(255) NOT NULL,
    hebrew_name VARCHAR(255) NOT NULL,
    gender VARCHAR(255) NOT NULL,
    email VARCHAR(255) NOT NULL,
    prayer_type VARCHAR(255) NOT NULL,
    created_at INT NOT NULL,
    dropped_at INT NOT NULL,
    user_track VARCHAR(255) NULL DEFAULT NULL,
    PRIMARY KEY (id),
    UNIQUE KEY `user_track` (`user_track`)
)";
        $wpdb->query($sql_dropped);
    }

    /**
     * 
     * @param WPCF7_ContactForm $form
     */
    public static function CF7OnSubmit($form)
    {
        $form_id = $form->id();
        if ($form_id == self::settings('form-registration')) {
            self::_handleRegistrationFormSubmit();
        } elseif ($form_id == self::settings('form-feedback')) {
            self::_handleFeedbackFormSubmit();
        }
    }

    private static function _handleRegistrationFormSubmit()
    {
        $gender = $_POST['gender'];
        $prayer_type = $_POST['davenfor'];
        $email = $_POST['your-email'];
        $fname = $_POST['first-name'];
        $lname = $_POST['last-name'];
        $hname = $_POST['hebrew-name'];

        try {
            $user_track = ShomeatefilaUser::generateUniqueTrack();
            $model = ShomeatefilaUser::create([
                'first_name' => $fname,
                'last_name' => $lname,
                'hebrew_name' => $hname,
                'gender' => $gender,
                'email' => $email,
                'prayer_type' => $prayer_type,
                'created_at' => time(),
                'user_track' => $user_track,
            ]);
            $model->addToPool();
            $model->autoMatch();
        } catch (MailchimpException $e) {
            addLogEntry("User {$email}\n".$e->getMessage()."\n".$e->getTraceAsString(),
                        'mailchimp.exception.registrationform');
        } catch (Exception $e) {
            
        }
    }

    private static function _handleFeedbackFormSubmit()
    {
        $user_track = $_POST['user_track'];
        $feedback_answer = $_POST['continue'];
        try {
            $user = ShomeatefilaUser::findByAttributes([
                'user_track' => $user_track,
            ]);
            if (!$user) {
                return;
            }
            $matchedUser = $user->findMatchedUser();
            if (!$matchedUser) {
                return;
            }
            $match = $user->getRelatedMatch();
            if ($feedback_answer == self::FEEDBACK_ANSWER_STOP) {
                // user stops - don't wait for partner's answer
                $user->makeDropped();
                $match->delete();
                if (!$matchedUser->autoMatch()) {
                    $matchedUser->makeArchived(Shomeatefila::settings('mailchimp-list-matched'));
                    $matchedUser->returnToPool();
                }
            } elseif (!$matchedUser->feedback_answer) {
                $user->feedback_answer = $feedback_answer;
                $user->update(['feedback_answer']);
                // waiting for partner answer, no action now
                return;
            } else {
                // here should be condition
                // "if ($matchedUser->feedback_answer == self::FEEDBACK_ANSWER_CONTINUE)"
                // but just in case - even if there are something different, we'll treat that the same way:
                // continue + continue, just reset pair time
                $matchedUser->feedback_answer = null;
                $matchedUser->update(['feedback_answer']);
                $match->updated_at = time();
                $match->update(['updated_at']);
            }

        } catch (MailchimpException $e) {
            addLogEntry("User #{$user->id} {$user->email}\n".$e->getMessage()."\n".$e->getTraceAsString(),
                        'mailchimp.exception.feedbackform');
        } catch (Exception $e) {
            
        }
    }

}
